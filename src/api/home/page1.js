import httpRequest from '@/request/index'
const baseUrl = process.env.BASE_URL;

export function getStaticData(date) {
  return httpRequest({
    url: "/dataScreen/monitoring/list/publishDate?publishDate=" + date,
    method: "get",
  });
}

export function getPictureData() {
  return httpRequest({
    url: "/dataScreen/monitoring/getMonthInfo",
    method: "get",
  });
}