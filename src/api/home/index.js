import request from "@/utils/request";

// 查询部门详细
export function getDept(deptId) {
  return request({
    url: "/system/dept/" + deptId,
    method: "get",
  });
}

// 新增部门
export function addDept(data) {
  return request({
    url: "/system/dept",
    method: "post",
    data: data,
  });
}

// 综合主题-综合信息
export function getZhInformation(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "mainTheme/getZhInformation",
      ...data,
    },
  });
}

// 流量信息臨時接口
export function getSfData(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "sfapi/getSfData",
      ...data,
    },
  });
}

// 流量信息臨時接口
export function getJamCountForToday(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "eventTheme/getJamCountForToday?dateType=day&isH=true",
      ...data,
    },
  });
}

// 流量信息臨時接口
export function eventGetList(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "event/getList",
      ...data,
    },
  });
}

// 获取关注事件列表
export function getSubscribeList(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "event/getSubscribeList",
      ...data,
    },
  });
}

// 获取待处理事件列表
export function getPendingList(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "event/getPendingList",
      ...data,
    },
  });
}
// 综合主题-突发事件、养护施工、恶劣天气
export function queryEvent(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "event/queryEvent",
      ...data,
    },
  });
}

// 当日救援信息
export function eventCtrlList(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "event/ctrl/getList",
      ...data,
    },
  });
}

// 当日救援信息
export function getDailyRescueData(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "alarmRescue/getDailyRescueData",
      ...data,
    },
  });
}

// 数智救援
export function getRealTimeRescueList(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "alarmRescue/getList",
      ...data,
    },
  });
}

// ，获取所有类型对应的父类型名称，用于组装事件图片名称
export function getParentNameMap(data) {
  return request({
    url: "/center/post",
    method: "post",
    data: {
      path: "eventType/getParentNameMap",
      ...data,
    },
  });
}

// ，获取所有类型对应的父类型名称，用于组装事件图片名称
// export function getParentNameMap(data) {
//   return request({
//     url: "/center/post",
//     method: "post",
//     data: {
//       path: "mainTheme/getWeather",
//       ...data,
//     },
//   });
// }
