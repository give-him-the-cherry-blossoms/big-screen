// import request from "@/utils/request";
import request from '@/request/index'

// 更新频率为一天的数据
export function dayInfo(deptId) {
  return request({
    url: "/dataScreen/citizenOpinion/dayInfo",
    method: "get",
  });
}
// 更新频率为5分钟的数据
export function fiveMinInfo(deptId) {
    return request({
      url: "/dataScreen/citizenOpinion/fiveMinInfo",
      method: "get",
    });
  }
  // 更新频率为一小时的数据
export function hourInfo(deptId) {
    return request({
      url: "/dataScreen/citizenOpinion/hourInfo",
      method: "get",
    });
  }
  // 低效率工单列表
export function listOrder(params) {
    // type	类型 1：已办结 2：未办结
    return request({
      url: "/dataScreen/citizenOpinion/listOrder",
      method: "get",
      params
    });
  }
    // 更新频率为1分钟的数据
export function oneMinInfo(params) {
    return request({
      url: "/dataScreen/citizenOpinion/oneMinInfo",
      method: "get",
    });
  }
    // 7日工单走势
export function orderCountTrend(params) {
    return request({
      url: "/dataScreen/citizenOpinion/orderCountTrend",
      method: "get",
    });
  }

// 更新频率为十五分钟的数据
export function fifteenMinInfo(params) {
  return request({
    url: "/dataScreen/roadCenter/get15MinData",
    method: "get",
  });
}
// 更新频率为一小时的数据
export function oneHoursInfo(params) {
  return request({
    url: "/dataScreen/roadCenter/get60MinData",
    method: "get",
  });
}
// 更新频率为30分中的数据
 export function threeteenMinInfo(params) {
  return request({
    url: "/dataScreen/roadCenter/get30MinData",
    method: "get",
  });
}
// 更新频率为5分钟的数据
 export function fiveNewMinInfo(params) {
  return request({
    url: "/dataScreen/roadCenter/get5MinData",
    method: "get",
  });
}