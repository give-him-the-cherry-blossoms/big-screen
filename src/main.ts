/*
 * @Author: yulinZ 1973329248@qq.com
 * @Date: 2024-05-28 23:16:25
 * @LastEditors: yulinZ 1973329248@qq.com
 * @LastEditTime: 2024-05-29 22:00:36
 * @FilePath: \daping_danao_web\src\main.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { createApp, provide } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.dark.css';

import '../public/css/textPointColor.css';
import '../public/css/common.css';
import * as echarts from 'echarts'
import 'echarts-gl';
import 'echarts-liquidfill';
// import 'swiper/swiper.scss';
import 'element-plus/dist/index.css'
import '@/assets/font/custom-font.css'


import highcharts from 'highcharts'
import highcharts3d from 'highcharts/highcharts-3d'
import AnimatedNumber from 'animated-number-vue3'
import Charts from "@/views/digScreen/components/charts";
import VueEchart from "@/views/digScreen/components/vue-echarts/index";
import VueHighChart from "@/views/digScreen/components/high-charts/index";
import VueScrollTable from "@/views/digScreen/components/table/index.vue";
import { scrollNumber } from "@/views/comprehensive/directives/scrollNum.js"

highcharts3d(highcharts)
const app: any = createApp(App).use(store).use(Antd).use(AnimatedNumber).use(router)
app.config.globalProperties.$echarts = echarts;
app.config.globalProperties.$highcharts = highcharts;
// 是否开启网络请求
const isOpenRequest = typeof(process.env.VUE_DATA_IS_REQUEST) === 'boolean'?process.env.VUE_DATA_IS_REQUEST:true;
// provide("isOpenRequest", isOpenRequest)
app.provide("isOpenRequest", isOpenRequest);
app.use(Charts);
import vue3SeamlessScroll from 'vue3-seamless-scroll';
app.use(vue3SeamlessScroll);
app.component("vue-echarts", VueEchart)
app.component("vue-high-charts", VueHighChart)
app.component("vue-scroll-table", VueScrollTable)
app.directive("scroll-num", scrollNumber);
app.mount('#app')
App.echarts = echarts
