import { createStore } from 'vuex'

export default createStore({
  state() {  
    return {  
      isAutoClick: false, // 是否手动点击
      basicsData:{} // 基础设施数据
    };  
  }, 
  getters: {
    // getClickStatus() {
    //   return this.state.isAutoClick
    // }
  },
  mutations: {
    changeAutoClick(state, flag) {
      if (this.state.isAutoClick !== flag) {
        this.state.isAutoClick = flag
      }
    },
    setBasicsData(state, data) {
     this.state.basicsData = data
    }
  },
  actions: {
  },
  modules: {
  }
})
