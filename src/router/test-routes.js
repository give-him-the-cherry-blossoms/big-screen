export default [
	{  
		path: "/test/port-shipping",
		name: "PortShipping",
		component: () => import(/* webpackChunkName: "about" */ "../views/comprehensive/zhys/module/port-shipping/index.vue")
	},
	// 自定义原子组件，最简单的样式
	{  
		path: "/customCompont",
		name: "PortShipping",
		component: () => import(/* webpackChunkName: "about" */ "../views/customCompont/index.vue")
	}
]