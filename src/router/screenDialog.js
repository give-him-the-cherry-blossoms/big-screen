const home = [
	{   //事件详情
		path: "/eventDetails",
		name: "eventDetails",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/eventDetails.vue"),
	},
	{   //事件详情
		path: "/commonLeftBottomDialog",
		name: "commonLeftBottomDialog",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/commonLeftBottomDialog.vue"),
	},
	{   //事件详情
		path: "/commonLeftBottomDialog1",
		name: "commonLeftBottomDialog1",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/commonLeftBottomDialog1.vue"),
	},
	{   //公共交通弹窗
		path: "/publicTransport",
		name: "publicTransport",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/publicTransport.vue"),
	},
	{   //公交关注事项列表
		path: "/busConcerns",
		name: "busConcerns",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/busConcerns.vue"),
	},
	{   //高速公路突发事件列表
		path: "/emergencyList",
		name: "emergencyList",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/emergencyList.vue"),
	},
	{	//渝中区突发事件列表
		path: "/yzEmergencyList",
		name: "yzEmergencyList",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/yzEmergencyList.vue"),
	},
	{	//公路舆情信息列表
		path: "/informationList",
		name: "informationList",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/informationList.vue"),
	},
	{	//网络舆情信息
		path: "/networkInfo",
		name: "networkInfo",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/networkInfo.vue"),
	},
	// 低效率工单详情
	{
		path: "/mqmygdInfo",
		name: "mqmygdInfo",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/mqmygdInfo.vue"),
	},
	{	//关注事项办理超期列表  今日群众关注事项
		path: "/completionRate",
		name: "completionRate",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/completionRate.vue"),
	},
	{	//关注事项办理差评列表
		path: "/negativeComment",
		name: "negativeComment",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/negativeComment.vue"),
	},
	{	//出租车投诉分布情况
		path: "/taxiComplaint",
		name: "taxiComplaint",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/taxiComplaint.vue"),
	},
	{	//投诉事项分布
		path: "/matterOfComplaint",
		name: "matterOfComplaint",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/matterOfComplaint.vue"),
	},
	{	//投诉情况详细列表
		path: "/peopleRightTop",
		name: "peopleRightTop",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/peopleRightTop.vue"),
	},
	{	//投诉情况详细列表
		path: "/consultList",
		name: "consultList",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/consultList.vue"),
	},
	{	//各模块当日累计数据详细列表
		path: "/everyModuleList",
		name: "everyModuleList",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/everyModuleList.vue"),
	},
	{	//关注事项数量今日趋势
		path: "/followEventList",
		name: "followEventList",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/followEventList.vue"),
	},
	{	//关注事项数量今日趋势
		path: "/itemConstituteList",
		name: "itemConstituteList",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/itemConstituteList.vue"),
	},
	{	//视频云
		path: "/videoCloud",
		name: "videoCloud",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/videoCloud.vue"),
	},
	{	//高速视频
		path: "/highVideo",
		name: "highVideo",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/highVideo.vue"),
	},
	{	//高速视频
		path: "/screenCenterRight",
		name: "screenCenterRight",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/screenCenterRight.vue"),
	},
	{
		path: "/leftTopTable",
		name: "leftTopTable",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/dialog/leftTopTable.vue"),
	},
	{
		path: "/screenRightLeft",
		name: "screenRightLeft",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/screenRightLeft.vue"),
	},
	{
		path: "/screenRightRight",
		name: "screenRightRight",
		component: () =>
			import(/* webpackChunkName: "about" */ "../views/digScreen/home/screenRightRight.vue"),
	},
]

export default [
	...home,
	{
		path: "/licensedCourier",
		name: "licensedCourier",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/postal/dialog/licensedCourier.vue"),
	},
	{
		path: "/postalService",
		name: "postalService",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/postal/dialog/postalService.vue"),
	},
	{
		path: "/philately",
		name: "philately",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/postal/dialog/philately.vue"),
	},
	{
		path: "/eventWarn",
		name: "eventWarn",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/postal/dialog/eventWarn.vue"),
	},
	{
		path: "/passenger",
		name: "passenger",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/civil/dialog/passenger.vue"),
	},
	{
		path: "/traffic",
		name: "traffic",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/civil/dialog/traffic.vue"),
	},
	{
		path: "/airport",
		name: "airport",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/civil/dialog/airport.vue"),
	},
	{
		path: "/diversionDialog",
		name: "diversionDialog",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/civil/dialog/diversionDialog.vue"),
	},
	{
		path: "/travelerDialog",
		name: "travelerDialog",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/civil/dialog/travelerDialog.vue"),
	},
	{
		path: "/travelerDetail",
		name: "travelerDetail",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/civil/dialog/travelerDetail.vue"),
	},
	{
		path: "/travelerDialog",
		name: "travelerDialog",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/civil/dialog/travelerDialog.vue"),
	},
	{
		path: "/civilCenterChart",
		name: "civilCenterChart",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/civil/dialog/civilCenterChart.vue"),
	},
	{
		path: "/railwayDialog",
		name: "railwayDialog",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/railway/dialog/railwayDialog.vue"),
	},
	// ##安全应急
	// 驾驶员基本信息
	{
		path: "/driverInfo",
		name: "driverInfo",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/aqyj/dialog/driverInfo.vue"),
	},
	// 车辆基本信息
	{
		path: "/carInfo",
		name: "carInfo",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/aqyj/dialog/carInfo.vue"),
	},
	// 区县信息
	{
		path: "/county",
		name: "county",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/aqyj/dialog/county.vue"),
	},
	// 企业基本信息
	{
		path: "/companyInfo",
		name: "companyInfo",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/aqyj/dialog/companyInfo.vue"),
	},
	// 道路危货运输高风险企业列表
	{
		path: "/dlwhysCompany",
		name: "dlwhysCompany",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/aqyj/dialog/dlwhysCompany.vue"),
	},
	// 年度道路危货运输安全隐患列表
	{
		path: "/dlwhysYear",
		name: "dlwhysYear",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/aqyj/dialog/dlwhysYear.vue"),
	},
	// ##交通执法
	//普通国省干线公路超限预警响应情况
	{
		path: "/roadWarning",
		name: "roadWarning",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/jtzf/dialog/roadWarning.vue"),
	},
	// 铁路里程
	{
		path: "/mileage",
		name: "mileage",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/railway/dialog/mileage.vue"),
	},
	{
		path: "/passengerDialog",
		name: "passengerDialog",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/railway/dialog/passengerDialog.vue"),
		// 铁路 货运里程
	}, {
		path: "/freight-volume",
		name: "freight-volume",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/railway/dialog/freight-volume.vue"),
	},
	//企业基本信息
	{
		path: "/jtzfCompanyInfo",
		name: "jtzfCompanyInfo",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/jtzf/dialog/companyInfo.vue"),
	},
	//车辆基本信息
	{
		path: "/jtzfCarInfo",
		name: "jtzfCarInfo",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/jtzf/dialog/carInfo.vue"),
	},
	// 审批信用 左
	{
		path: "/spxyLeft",
		name: "spxyLeft",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/spxy/leftAll.vue"),
	},
	// 审批信用 中
	{
		path: "/spxyCenter",
		name: "spxyCenter",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/spxy/center.vue"),
	},
	// 审批信用 右
	{
		path: "/spxyRight",
		name: "spxyRight",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/spxy/rightAll.vue"),
	},
	{
		path: "/waterLevel",
		name: "waterLevel",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/home/dialog/waterLevel.vue"),
	},
	{
		path: "/showPPT",
		name: "showPPT",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/home/dialog/showPPT.vue"),
	},
	{
		path: "/ppt1",
		name: "ppt1",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/home/dialog/ppt1.vue"),
	},
	{
		path: "/ppt2",
		name: "ppt2",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/home/dialog/ppt2.vue"),
	},
	{
		path: "/nav",
		name: "nav",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/home/nav.vue"),
	},
	//新首页
	{
		path: "/newHomeLeft",
		name: "newHomeLeft",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/newHome/newLeft.vue"),
	},
	//总驾驶舱中间上
	{
		path: "/eventInformationDialog",
		name: "eventInformationDialog",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/newHome/center/eventInformationDialog.vue"),
	},
	{
		path: "/newHomeCenterTop",
		name: "newHomeCenterTop",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/newHome/center/newHomeCenterTop.vue"),
	},
	//总驾驶舱中间左上
	{
		path: "/newHomeCenterTopLeft",
		name: "newHomeCenterTopLeft",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/newHome/center/newHomeCenterTopLeft.vue"),
	},
	//总驾驶舱中间右上
	{
		path: "/newHomeCenterTopRight",
		name: "newHomeCenterTopRight",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/newHome/center/newHomeCenterTopRight.vue"),
	},
	//总驾驶舱中间下
	{
		path: "/newHomeCenterBottom",
		name: "newHomeCenterBottom",
		component: () =>
			import( /* webpackChunkName: "about" */ "@/views/digScreen/newHome/center/newHomeCenterBottom.vue"),
	},
		
]
