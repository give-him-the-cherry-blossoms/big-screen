import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router"
import dialogs from "./screenDialog.js"
import HomeView from "../views/HomeView.vue"
import Page from "../views/layout/page.vue"
import Zhzf from "@/views/layout/zhzf.vue"
import Aqgl from "@/views/layout/aqgl.vue"
import TestRoutes from "./test-routes.js"
import TRoutes from "./t-routes.js"
import newScreen from "./newScreen.js"
// @ts-ignore
const routes: Array<RouteRecordRaw> = [
    ...TestRoutes,
    ...dialogs,
    ...TRoutes,
    ...newScreen,
    //路由配置
    {
        path: "/home",
        name: "home",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/home/index.vue"),
    },
    {
        path: "/leftZhgl",
        name: "leftZhgl",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/zhgl/leftZhgl.vue"
            ),
    },
    {
        path: "/rightZhgl",
        name: "rightZhgl",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/zhgl/rightZhgl.vue"
            ),
    },
    //数字公路
    {
        path: "/leftJcssgl",
        name: "leftJcssgl",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcssgl/leftJcssgl.vue"
            ),
    },
    {
        path: "/rightJcssgl",
        name: "rightJcssgl",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcssgl/rightJcssgl.vue"
            ),
    },
    {
        path: "/midJcssgl",
        name: "midJcssgl",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcssgl/midJcssgl.vue"
            ),
    },
    //交通应急资源
    {
        path: "/jtyjzyLeft",
        name: "jtyjzyLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/aqyz/jtyjzy/jtyjzyLeft.vue"
            ),
    },
    {
        path: "/jtyjzyRight",
        name: "jtyjzyRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/aqyz/jtyjzy/jtyjzyRight.vue"
            ),
    },

    //数字民航
    {
        path: "/szmhLeft",
        name: "szmhLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szmh/MhLeft.vue"
            ),
    },
    {
        path: "/szmhRight",
        name: "szmhRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szmh/MhRight.vue"
            ),
    },
    {
        path: "/szmhTable",
        name: "szmhTable",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szmh/MhTable.vue"
            ),
    },
    // 机场信息弹窗
    {
        path: "/AirInfoPopupContent",
        name: "AirInfoPopupContent",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szmh/AirInfoPopupContent.vue"
            ),
    },
    // 机场运输量弹窗
    {
        path: "/TransportPopupContent",
        name: "TransportPopupContent",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szmh/TransportPopupContent.vue"
            ),
    },

    //客运服务
    {
        path: "/kyfwLeft",
        name: "kyfwLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/ysfw/kyfw/kyfwLeft.vue"
            ),
    },
    {
        path: "/kyfwRight",
        name: "kyfwRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/ysfw/kyfw/kyfwRight.vue"
            ),
    },

    //公共交通
    {
        path: "/ggjtLeft",
        name: "ggjtLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/ysfw/ggjt/ggjtLeft.vue"
            ),
    },
    {
        path: "/ggjtRight",
        name: "ggjtRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/ysfw/ggjt/ggjtRight.vue"
            ),
    },

    //审批
    {
        path: "/spLeft",
        name: "spLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/spxy/sp/spindex.vue"
            ),
    },
    {
        path: "/spRight",
        name: "spRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/spxy/sp/spindex_r.vue"
            ),
    },

    //数字邮政
    {
        path: "/szyzLeft",
        name: "szyzLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szyz/left.vue"
            ),
    },
    {
        path: "/szyzRight",
        name: "szyzRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szyz/right.vue"
            ),
    },
    //数字邮政-业务量弹窗
    {
        path: "/szyzPopupYwl",
        name: "szyzPopupYwl",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szyz/PopupContentYwl.vue"
            ),
    },
    //数字邮政-业务收入弹窗
    {
        path: "/szyzPopupYwsr",
        name: "szyzPopupYwsr",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szyz/PopupContentYwsr.vue"
            ),
    },
    //数字邮政-网点数弹窗
    {
        path: "/szyzPopupWds",
        name: "szyzPopupWds",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szyz/PopupContentWds.vue"
            ),
    },

    //信用
    {
        path: "/xyleft",
        name: "xyleft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/spxy/xy/xyleft.vue"
            ),
    },
    {
        path: "/xyright",
        name: "xyright",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/spxy/xy/xyright.vue"
            ),
    },
    {
        path: "/xymiddle",
        name: "xymiddle",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/spxy/xy/xymiddle.vue"
            ),
    },

    //高速
    {
        path: "/cxgsLeft",
        name: "cxgsLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/cxgs/cxgsLeft.vue"
            ),
    },
    {
        path: "/cxgsMiddle",
        name: "cxgsMiddle",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/cxgs/cxgsMiddle.vue"
            ),
    },
    {
        path: "/cxgsRight",
        name: "cxgsRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/cxgs/cxgsRight.vue"
            ),
    },

    //平安交通指数
    {
        path: "/pajtzsLeft",
        name: "pajtzsLeft",
        component: () => import("../views/hyzz/aqyj/pajtzs/pajtzsLeft.vue"),
    },
    {
        path: "/pajtzsRight",
        name: "pajtzsRight",
        component: () => import("../views/hyzz/aqyj/pajtzs/pajtzsRight.vue"),
    },
    {
        path: "/pajtzsMiddle",
        name: "pajtzsMiddle",
        component: () => import("../views/hyzz/aqyj/pajtzs/pajtzsMiddle.vue"),
    },
    //较大事故弹窗
    {
        path: "/PopupContentJdsg",
        name: "PopupContentJdsg",
        component: () =>
            import("../views/hyzz/aqyj/pajtzs/PopupContentJdsg.vue"),
    },
    //综合执法
    {
        path: "/zhzfLeft",
        name: "zhzfLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/zhzf/zhzfLeft.vue"
            ),
    },
    {
        path: "/zhzfRight",
        name: "zhzfRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/zhzf/zhzfRight.vue"
            ),
    },
    //渝运安
    {
        path: "/fxgzLeft",
        name: "fxgzLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/fxgz/fxgzLeft.vue"
            ),
    },
    {
        path: "/fxgzRight",
        name: "fxgzRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/hyzz/fxgz/fxgzRight.vue"
            ),
    },
    // {
    //   path: "/yya",
    //   name: "yya",
    //   component: () =>
    //     import(
    //           /* webpackChunkName: "about" */ "../views/hyzz/fxgz/yya.vue"
    //     ),
    // },
    //货运服务
    {
        path: "/hyfwLeft",
        name: "hyfwLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/ysfw/hyfw/hyfwLeft.vue"
            ),
    },
    {
        path: "/hyfwRight",
        name: "hyfwRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/ysfw/hyfw/hyfwRight.vue"
            ),
    },

    //数字港航
    {
        path: "/szghLeft",
        name: "/szghLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szgh/szghLeft.vue"
            ),
    },
    {
        path: "/szghCenter",
        name: "/szghCenter",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szgh/szghCenter.vue"
            ),
    },
    {
        path: "/szghRight",
        name: "/szghRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szgh/szghRight.vue"
            ),
    },
    {
        path: "/szghPage",
        name: "/szghPage",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/jcss/szgh/szghPage.vue"
            ),
    },

    //数字铁路
    {
        path: "/leftsztl",
        name: "leftsztl",
        component: () => import("../views/jcss/sztl/leftSztl.vue"),
    },
    {
        path: "/midsztl",
        name: "midsztl",
        component: () => import("../views/jcss/sztl/midSztl.vue"),
    },
    {
        path: "/rightsztl",
        name: "rightsztl",
        component: () => import("../views/jcss/sztl/rightSztl.vue"),
    },

    //管养处-首页
    {
        path: "/gysyLeft",
        name: "gysyLeft",
        component: () => import("../views/cssc/gysy/gysyLeft.vue"),
    },
    {
        path: "/gysyRight",
        name: "gysyRight",
        component: () => import("../views/cssc/gysy/gysyRight.vue"),
    },
    //管养处-收费
    {
        path: "/sfLeft",
        name: "sfLeft",
        component: () => import("../views/cssc/sf/sfLeft.vue"),
    },
    {
        path: "/sfRight",
        name: "sfRight",
        component: () => import("../views/cssc/sf/sfRIght.vue"),
    },
    //管养处-养护
    {
        path: "/gyyhLeft",
        name: "gyyhLeft",
        component: () => import("../views/cssc/gyyh/gyyhLeft.vue"),
    },
    {
        path: "/gyyhRight",
        name: "gyyhRight",
        component: () => import("../views/cssc/gyyh/gyyhRight.vue"),
    },
    //管养处-路政
    {
        path: "/lzLeft",
        name: "lzLeft",
        component: () => import("../views/cssc/gylz/lzLeft.vue"),
    },
    {
        path: "/lzRight",
        name: "lzRight",
        component: () => import("../views/cssc/gylz/lzRight.vue"),
    },
    //管养处-运行
    {
        path: "/yxLeft",
        name: "yxLeft",
        component: () => import("../views/cssc/gyyx/yxLeft.vue"),
    },
    {
        path: "/yxRight",
        name: "yxRight",
        component: () => import("../views/cssc/gyyx/yxRight.vue"),
    },
    //建管处视窗
    {
        path: "/jgcssLeft",
        name: "jgcssLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/csss/jgcsc/leftBox.vue"
            ),
    },
    {
        path: "/jgcssRight",
        name: "jgcssRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/csss/jgcsc/rightBox.vue"
            ),
    },
    //科技处-start
    {
        path: "/kjcLeft",
        name: "kjcLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/kjc/kjcLeft.vue"
            ),
    },
    {
        path: "/kjcRight",
        name: "kjcRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/kjc/kjcRight.vue"
            ),
    },
    {
        path: "/kjcMid",
        name: "kjcMid",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/kjc/kjcMid.vue"
            ),
    },
    //规划处-start
    //规划处-首页
    {
        path: "/ghcsyLeft",
        name: "ghcsyLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/sy/syLeft.vue"
            ),
    },
    {
        path: "/ghcsyRight",
        name: "ghcsyRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/sy/syRight.vue"
            ),
    },
    //规划处-项目
    {
        path: "/xmLeft",
        name: "xmLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/xm/xmLeft.vue"
            ),
    },
    {
        path: "/xmRight",
        name: "xmRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/xm/xmRight.vue"
            ),
    },
    //规划处视窗-运输
    {
        path: "/ysLeft",
        name: "ysLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/ys/ysLeft.vue"
            ),
    },
    {
        path: "/ysRight",
        name: "ysRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/ys/ysRight.vue"
            ),
    },
    //规划处-实时路况
    {
        path: "/sslkLeft",
        name: "sslkLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/sslk/sslkLeft.vue"
            ),
    },
    //优化
    {
        path: "/leftsslk",
        name: "leftsslk",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/sslk/leftsslk.vue"
            ),
    },
    {
        path: "/rightSslk",
        name: "rightSslk",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/sslk/rightSslk.vue"
            ),
    },
    {
        path: "/sslkRight",
        name: "sslkRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/sslk/sslkRight.vue"
            ),
    },
    //规划处-年度统计
    {
        path: "/ndtjLeft",
        name: "ndtjLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/ndtj/ndtjLeft.vue"
            ),
    },
    {
        path: "/ndtjRight",
        name: "ndtjRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/ndtj/ndtjRight.vue"
            ),
    },
    {
        path: "/ndtjMiddle",
        name: "ndtjMiddle",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/ghc/ndtj/ndtjMiddle.vue"
            ),
    },

    {
        path: "/comprehensiveTitle",
        name: "综合页标题",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/comprehensiveTitle.vue"
            ),
    },
    {
        path: "/comprehensiveLeft",
        name: "综合页左图表",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/comprehensiveLeft.vue"
            ),
    },

    {
        path: "/comprehensiveRight",
        name: "综合页右图表",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtssAndYs/right.vue"
            ),
    },
    {
        path: "/comprehensiveCenter",
        name: "综合页数据",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/comprehensiveCenter.vue"
            ),
    },
    {
        path: "/kjcMid",
        name: "kjcMid",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/cssc/kjc/kjcMid.vue"
            ),
    },
    {
        path: "/screenHeader",
        name: "screenHeader",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/home/screenHeader.vue"
            ),
    },
    {
        path: "/screenCenterLeft",
        name: "screenCenterLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/home/screenCenterLeft.vue"
            ),
    },
    {
        path: "/screenLeftTop",
        name: "screenLeftTop",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/home/screenLeftTop.vue"
            ),
    },
    {
        path: "/screenLeftBottom",
        name: "screenLeftBottom",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/home/screenLeftBottom.vue"
            ),
    },
    {
        path: "/screenRightBottom",
        name: "screenRightBottom",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/home/screenRightBottom.vue"
            ),
    },
    {
        path: "/supervise",
        name: "supervise",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/home/supervise.vue"
            ),
    },
    {
        path: "/screenRightTop",
        name: "screenRightTop",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/home/screenRightTop.vue"
            ),
    },
    {
        path: "/ysjcR",
        name: "ysjcR",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/ysjc/right.vue"
            ),
    },
    {
        path: "/waterLeft",
        name: "水运左侧",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/water/left.vue"
            ),
    },
    {
        path: "/civilLeftTop",
        name: "civilLeftTop",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/civil/leftTop.vue"
            ),
    },
    {
        path: "/civilLeft",
        name: "civilLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/civil/left.vue"
            ),
    },
    {
        path: "/civilRight",
        name: "civilRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/civil/civilRight.vue"
            ),
    },
    {
        path: "/civilLeftBottom",
        name: "civilLeftBottom",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/civil/leftBottom.vue"
            ),
    },
    {
        path: "/civilCenterTop",
        name: "civilCenterTop",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/civil/centerTop.vue"
            ),
    },
    {
        path: "/civilCenterBottom",
        name: "civilCenterBottom",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/civil/centerBottom.vue"
            ),
    },
    {
        path: "/civilRightTop",
        name: "civilRightTop",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/civil/rightTop.vue"
            ),
    },
    {
        path: "/civilRightBottom",
        name: "civilRightBottom",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/civil/rightBottom.vue"
            ),
    },
    {
        path: "/railwayRight",
        name: "railwayRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/railway/railwayRight.vue"
            ),
    },
    {
        path: "/jtzfLeftTopOne",
        name: "jtzfLeftTopOne",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/leftTopOne.vue"
            ),
    },
    {
        path: "/jtzfLeftTopTwo",
        name: "jtzfLeftTopTwo",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/leftTopTwo.vue"
            ),
    },
    {
        path: "/jtzfLeftBottomOne",
        name: "jtzfLeftBottomOne",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/leftBottomOne.vue"
            ),
    },
    {
        path: "/jtzfLeftBottomTwo",
        name: "jtzfLeftBottomTwo",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/leftBottomTwo.vue"
            ),
    },
    {
        path: "/jtzfLeftBottomThree",
        name: "jtzfLeftBottomThree",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/leftBottomThree.vue"
            ),
    },
    {
        path: "/jtzfRightTopOne",
        name: "jtzfRightTopOne",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/rightTopOneOld.vue"
            ),
    },
    {
        path: "/jtzfRightTopTwo",
        name: "jtzfRightTopTwo",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/rightTopTwo.vue"
            ),
    },
    {
        path: "/jtzfRightBottomOne",
        name: "jtzfRightBottomOne",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/rightBottomOne.vue"
            ),
    },
    {
        path: "/jtzfRightBottomTwo",
        name: "jtzfRightBottomTwo",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/rightBottomTwo.vue"
            ),
    },
    {
        path: "/jtzfRightBottomThree",
        name: "jtzfRightBottomThree",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/rightBottomThree.vue"
            ),
    },
    {
        path: "/jtzfCenterBottom",
        name: "jtzfCenterBottom",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/centerBottom.vue"
            ),
    },
    {
        path: "/jtzfLeft",
        name: "jtzfLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/leftAll.vue"
            ),
    },
    {
        path: "/jtzfRight",
        name: "jtzfRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/jtzf/rightAll.vue"
            ),
    },
    {
        path: "/jtzfCenterTop",
        name: "jtzfCenterTop",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "@/views/digScreen/jtzf/centerTop.vue"
            ),
    },
    {
        path: "/aqyjLeftOne",
        name: "aqyjLeftOne",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/aqyj/leftOne.vue"
            ),
    },
    {
        path: "/aqyjLeftTop",
        name: "aqyjLeftTop",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/aqyj/leftTop.vue"
            ),
    },
    {
        path: "/aqyLeftBottom",
        name: "aqyLeftBottom",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/aqyj/leftBottom.vue"
            ),
    },
    {
        path: "/aqyjRightLast",
        name: "aqyjRightLast",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/aqyj/rightLast.vue"
            ),
    },
    {
        path: "/aqyjRightTop",
        name: "aqyjRightTop",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/aqyj/rightTop.vue"
            ),
    },
    {
        path: "/aqyjRightBottom",
        name: "aqyjRightBottom",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/aqyj/rightBottom.vue"
            ),
    },
    {
        path: "/aqyjLeft",
        name: "aqyjLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/aqyj/leftAll.vue"
            ),
    },
    {
        path: "/aqyjRight",
        name: "aqyjRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/aqyj/rightAll.vue"
            ),
    },
    {
        path: "/aqyjCenterTop",
        name: "aqyjCenterTop",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "@/views/digScreen/aqyj/centerTop.vue"
            ),
    },
    {
        path: "/postalRight",
        name: "postalRight",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "@/views/digScreen/postal/home/postalRight.vue"
            ),
    },
    {
        path: "/postalLeft",
        name: "postalLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "@/views/digScreen/postal/home/postalLeft.vue"
            ),
    },

    {
        path: "/railwayLeft",
        name: "railwayLeft",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/digScreen/railway/railwayLeft.vue"
            ),
    },
    {
        path: "/jtysLeft2",
        name: "交通设施与运输左侧2",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/left2.vue"
            ),
    },
    {
        path: "/jtysLeft",
        name: "交通设施与运输左侧",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/left3.vue"
            ),
    },
    {
        path: "/jtysRight",
        name: "交通设施与运输右侧",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/right3.vue"
            ),
    },
    {
        path: "/jtysRight2",
        name: "交通设施与运输右侧2",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/right2.vue"
            ),
    },
    {
        path: "/jtysItem1",
        name: "交通设施与运输模块一",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/module/item1.vue"
            ),
    },
    {
        path: "/jtysItem2",
        name: "交通设施与运输模块二",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/module/item2.vue"
            ),
    },
    {
        path: "/jtysItem3",
        name: "交通设施与运输模块三",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/module/item3.vue"
            ),
    },
    {
        path: "/jtysItem4",
        name: "交通设施与运输模块四",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/module/item4.vue"
            ),
    },
    {
        path: "/jtysItem5",
        name: "交通设施与运输模块五",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/module/item5.vue"
            ),
    },
    {
        path: "/jtysItem6",
        name: "交通设施与运输模块六-下钻指标",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jtysAndYs/module/item6.vue"
            ),
    },
    {
        path: "/basicLeft",
        name: "公路左侧",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/highway/left.vue"
            ),
    },
    {
        path: "/basicLeftNew",
        name: "公路左侧2",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/highwayNew/left.vue"
            ),
    },
    {
        path: "/basicRight",
        name: "公路右侧",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/highway/right.vue"
            ),
    },
    {
        path: "/basicRightNew",
        name: "公路右侧2",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/highwayNew/right.vue"
            ),
    },
    {
        path: "/basicTable",
        name: "公路中间表格",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/highway/tablePage.vue"
            ),
    },
    {
        path: "/modal1",
        name: "弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal1.vue"
            ),
    },
    {
        path: "/modal2",
        name: "高速公路突发事件",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal2.vue"
            ),
    },
    {
        path: "/modalDetails2",
        name: "高速公路突发事件详情",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modalDetails2.vue"
            ),
    },
    {
        path: "/modal3",
        name: "高速公路4级及以上突发事件列表",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal3.vue"
            ),
    },
    {
        path: "/modal4",
        name: "国省干线且断起数超60详情",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal4.vue"
            ),
    },
    {
        path: "/modalDetails",
        name: "国省干线详情",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modalDetails.vue"
            ),
    },
    {
        path: "/modal5",
        name: "水运-船舶检验",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal5.vue"
            ),
    },
    {
        path: "/modal6",
        name: "水运-船舶报废强制预警",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal6.vue"
            ),
    },
    {
        path: "/modal7",
        name: "高速列表",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal7.vue"
            ),
    },
    {
        path: "/modal8",
        name: "换电站信息统计",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal8.vue"
            ),
    },
    {
        path: "/modal10",
        name: "加气站信息",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal10.vue"
            ),
    },
    {
        path: "/modal11",
        name: "司机之家信息",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal11.vue"
            ),
    },
    {
        path: "/modal12",
        name: "充电桩",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal12.vue"
            ),
    },
    {
        path: "/modal13",
        name: "三峡游当日实时客运量",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal13.vue"
            ),
    },
    {
        path: "/modal14",
        name: "两江游当日实时客运量",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal14.vue"
            ),
    },
    {
        path: "/modal15",
        name: "上月国省干线交通拥挤度top10",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal15.vue"
            ),
    },
    {
        path: "/modal16",
        name: "四五类桥梁列表",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal16.vue"
            ),
    },
    {
        path: "/modal17",
        name: "四五类隧道列表",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal17.vue"
            ),
    },
    {
        path: "/waterTag",
        name: "水运-中间按钮组",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/water/waterTag.vue"
            ),
    },
    {
        path: "/linkLeft",
        name: "重点应用左侧",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/appKeyLeft.vue"
            ),
    },
    {
        path: "/linkRight",
        name: "重点应用右侧",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/appKeyRight.vue"
            ),
    },
    {
        path: "/appKey",
        name: "重点应用",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/appKey.vue"
            ),
    },
    {
        path: "/appKeyImg",
        name: "重点应用-图片展示",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/appKeyImg.vue"
            ),
    },
    {
        path: "/doorLeft",
        name: "重点应用2-左侧",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/door/appKeyLeft.vue"
            ),
    },
    {
        path: "/doorRight",
        name: "重点应用2-右侧",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/door/appKeyRight.vue"
            ),
    },
    {
        path: "/doorBottom",
        name: "重点应用2-底部",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/door/appKeyBottom.vue"
            ),
    },

    {
        path: "/ysjtModal1",
        name: "运输-昨日出租客流总量弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/modal1.vue"
            ),
    },
    {
        path: "/ysjtModal2",
        name: "运输-巡游出租车实载率弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/modal2.vue"
            ),
    },
    {
        path: "/ysjtModal3",
        name: "运输-巡游出租车稳定度弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/modal3.vue"
            ),
    },
    {
        path: "/ysjtModal4",
        name: "运输-网约车平台数弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/modal4.vue"
            ),
    },
    {
        path: "/ysjtModal5",
        name: "运输-网约车饱和度弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/modal5.vue"
            ),
    },
    {
        path: "/ysjtModal6",
        name: "运输-网约车订单弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/modal6.vue"
            ),
    },
    {
        path: "/ysjtModal7",
        name: "运输-昨日网约客流总量弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/modal7.vue"
            ),
    },
    {
        path: "/ysjtModal8",
        name: "公交-昨日网约客流总量弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/modal8.vue"
            ),
    },
    {
        path: "/ysjtModal10",
        name: "运输-西站预警查看弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/modal10.vue"
            ),
    },
    {
        path: "/yjModal",
        name: "运输-西站预警弹窗",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/ys/yjModal.vue"
            ),
    },
    {
        path: "/syModal18",
        name: "水上停航",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/modal18.vue"
            ),
    },
    {
        path: "/glSjListModal",
        name: "公路事件",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/modal/commonLeftBottomDialog.vue"
            ),
    },
    {
        path: "/kyLeft",
        name: "客运左边",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/ky/left.vue"
            ),
    },
    {
        path: "/kyRight",
        name: "客运右边",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/ky/right.vue"
            ),
    },
    {
        path: "/slRight",
        name: "水路右边",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/sl/right.vue"
            ),
    },
    {
        path: "/slLeft",
        name: "水路左边",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/sl/left.vue"
            ),
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/page1",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/mqmyleft.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/page2",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/page2.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/page3",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/page3.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/page4",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/page4.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/page5",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/page5.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/page6",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/page6.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/page7",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/page7.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/page8",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/page8.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/freight",
                name: 'freight',
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/freight/index.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/passengerTransport",
                name: "passengerTransport",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/passengerTransport/index.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/freight1",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/freight/components/freight1.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/freight2",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/freight/components/freight2.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/freight3",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/freight/components/freight3.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/freight4",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/freight/components/freight4.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/passengerTransport1",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/passengerTransport/components/passengerTransport1.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/passengerTransport2",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/passengerTransport/components/passengerTransport2.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/passengerTransport3",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/passengerTransport/components/passengerTransport3.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/passengerTransport4",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/passengerTransport/components/passengerTransport4.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/passengerTransport5",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/passengerTransport/components/passengerTransport5.vue"
                    ),
            },
        ],
    },
    {
        path: "/zhIndex",
        name: "核心业务页面",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/zhys/index.vue"
            ),
    },
    {
        path: "/jscRight2",
        name: "驾驶舱右边页面",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jsc/right.vue"
            ),
    },
    {
        path: "/jscRight",
        name: "驾驶舱右边页面2",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/comprehensive/jsc2/right.vue"
            ),
    },
    {
        path: "/",
        component: Zhzf,
        children: [
            {
                path: "/zhzf1",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/zhzf/page1.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Zhzf,
        children: [
            {
                path: "/zhzf2",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/zhzf/page2.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Zhzf,
        children: [
            {
                path: "/zhzf4",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/zhzf/page4.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Zhzf,
        children: [
            {
                path: "/zhzf3",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/zhzf/page3.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Zhzf,
        children: [
            {
                path: "/zhzf5",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/zhzf/page5.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Zhzf,
        children: [
            {
                path: "/zhzf6",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/zhzf/page6.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Aqgl,
        children: [
            {
                path: "/aqgl4",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/aqgl/page2.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Aqgl,
        children: [
            {
                path: "/aqgl5",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/home/components/aqgl/page5.vue"
                    ),
            },
        ],
    },
    {
        path: "/anglzhy",
        name: "anglzhy",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/home/anglzhye.vue"
            ),
    },
    {
        path: "/zhzfzhy",
        name: "zhzfzhy",
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/home/zhzfzhye.vue"
            ),
    },



    //8-29//行业智治
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/Intelligent",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/Intelligent/index.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/Transportation",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/Transportation/index.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/CreditApproval",
                name:'CreditApproval',
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/CreditApproval/index.vue"
                    ),
            },
        ],
    },
    {
        path: "/",
        component: Page,
        children: [
            {
                path: "/CreditApproval1",
                component: () =>
                    import(
                        /* webpackChunkName: "about" */ "../views/CreditApproval/components/right.vue"
                    ),
            },
        ],
    },
   
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})
router.beforeEach((to, from, next) => {
    // console.log(to,from, 'toto719231')
    if (to.fullPath == '/screenHome' && from.fullPath !== '/') {
        for (let i = 0; i < 100; i++) {
            clearInterval(i)
        }
        next()
    } else {
        next()
    }

})
export default router
