export default [
    {
        path: "/screenHome",
        name: "screenHome",
        component: () => import(/* webpackChunkName: "about" */ "../views/digScreen/tHome/screenHome.vue")
    },
    //邮政管理P3

    {
        path: "/yzglP3",
        name: "yzglP3",
        component: () =>
            import( /* webpackChunkName: "about" */ "@/views/digScreen/tHome/yzglP3/index.vue"),
    },

    {
        path: "/jtyxP2",
        name: "jtyxP2",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/digScreen/tHome/jtyxP2/index.vue"),
    },
    {
        path: "/glglP3",
        name: "glglP3",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/digScreen/tHome/glglP3/index.vue"),
    },
    {
        path: "/glglP3Copy",
        name: "glglP3Copy",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/digScreen/tHome/glglP3Copy/index.vue"),
    },
    {
        path: "/mhglP3",
        name: "mhglP3",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/digScreen/tHome/mhglP3/index.vue"),
    },
    {
        path: "/jcssP2",
        name: "jcssP2",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/digScreen/tHome/jcssP2/index.vue"),
    },
    {
        path: "/ghglP3",
        name: "ghglP3",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/digScreen/tHome/ghglP3/index.vue"),
    },
    {
        path: "/tlglP3",
        name: "tlglP3",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/digScreen/tHome/tlglP3/index.vue"),
    },
    {
        path: "/tabTopKPI",
        name: "tabTopKPI",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/digScreen/newHome/center/tabTopKPI.vue"),
    },
]
