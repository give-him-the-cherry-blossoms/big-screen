//综合执法请求地址
import {
  ZHZFXZCF,
  ZHZFXZQZ,
  ZHZFTSCL,
  XZJC,
  GLZC,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//综合执法-行政处罚
export async function getxzcf(params) {
  return request(BASE_URL+ZHZFXZCF, METHOD.GET, params)
}
//综合执法-行政强制
export async function getxzqz(params) {
  return request(BASE_URL+ZHZFXZQZ, METHOD.GET, params)
}
//综合执法-行政强制
export async function gettscl(params) {
  return request(BASE_URL+ZHZFTSCL, METHOD.GET, params)
}
//综合执法-行政检查
export async function getxzjc(params) {
  return request(BASE_URL+XZJC, METHOD.GET, params)
}
//综合执法-公路治超
export async function getglzc(params) {
  return request(BASE_URL+GLZC, METHOD.GET, params)
}
export default {
  getxzcf,
  getxzqz,
  gettscl,
  getxzjc,
  getglzc,
}
