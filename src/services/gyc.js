//管养处
import {
  GYCDRRKLL,
  GYCTFSJ,
  GYCGZQK,
  GYCJTZS,
  GYCYYQK,
  MKGJZ, GYCSFJYDW, KJCSSCYYGMYS, getTransport, getHighWayTollRankTop,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//首页-流量信息
export async function getdrrkll(params) {
  return request(BASE_URL+GYCDRRKLL, METHOD.GET, params)
}
export async function getRevenueSituation(params) {
  return request(BASE_URL+GYCSFJYDW, METHOD.GET, params)
}
export async function getTransportationScale(params) {
  return request(BASE_URL+KJCSSCYYGMYS, METHOD.GET, params)
}
// 规划处-实时路况-客运监测-出租车、网约车、联网售票（指标）
export async function getTransportPassenger(params) {
  return request(BASE_URL+getTransport, METHOD.GET, params)
}
//管养处-收费-收费单位公路排名(列表)
export async function getHighWayTollRankTopData(params) {
  return request(BASE_URL+getHighWayTollRankTop, METHOD.GET, params)
}
//首页-突发事件
export async function gettfsj(params) {
  return request(BASE_URL+GYCTFSJ, METHOD.GET, params)
}
//首页-管制情况
export async function getgzqk(params) {
  return request(BASE_URL+GYCGZQK, METHOD.GET, params)
}
//运行-管制情况
export async function getgycjtzs(params) {
  return request(BASE_URL+GYCJTZS, METHOD.GET, params)
}
export async function getgycyyqk(params) {
  return request(BASE_URL+GYCYYQK, METHOD.GET, params)
}
export async function getDataByKeys(params) {
  return request(BASE_URL+MKGJZ, METHOD.GET, params)
}
export default {
  getdrrkll,
  gettfsj,
  getgzqk,
  getgycjtzs,
  getDataByKeys,
}
