//数字港航请求地址
import {
  SZGHSYJS,
  SZGHHDYH,
  SZGHDNZDXM,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//数字港航-水运建设(饼图和指标)
export async function getszghsyjs(params) {
  return request(BASE_URL+SZGHSYJS+'?type=3', METHOD.GET, params)
}
//数字港航-柱状图
export async function getszghhdyh(params) {
  return request(BASE_URL+SZGHHDYH, METHOD.GET, params)
}
//数字港航-列表
export async function getszghdnzdxm(params) {
  return request(BASE_URL+SZGHDNZDXM, METHOD.GET, params)
}
export default {
  getszghsyjs,
  getszghhdyh,
  getszghdnzdxm,
}
