//数字民航请求地址
import {
  SZMHDNZDXM,
  SZMHMHJS,
  JiangBei
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//数字民航-列表
export async function getszmhdnzdxm(params) {
  return request(BASE_URL+SZMHDNZDXM, METHOD.GET, params)
}
//数字民航-民航建设
export async function getszmhmhjs(params) {
  return request(BASE_URL+SZMHMHJS+'?type=4', METHOD.GET, params)
}
//数字民航-月度起飞航班数、月度登机人数
export async function getszmhJiangBei(params) {
  return request(BASE_URL+JiangBei, METHOD.GET, params)
}
export default {
  getszmhdnzdxm,
  getszmhmhjs,
  getszmhJiangBei
}
