import { request, METHOD } from "@/utils/request";

const BASE_URL = process.env.VUE_APP_URL;

//page56
export const getPage56 = async () => {
  return request(BASE_URL + "/dataScreen/transport/road", METHOD.GET);
};

export const getPage6 = async () => {
  return request(BASE_URL + "/dataScreen/transport/water", METHOD.GET);
};

//综合概览-成渝双城经济圈
export const getZongheCydcjjq = async (params) => {
  return request(BASE_URL + "/dataScreen/zonghe/cydcjjq", METHOD.GET, params);
};

//综合概览-服务能力
export const getZongheFwnl = async (params) => {
  return request(BASE_URL + "/dataScreen/zonghe/fwnl", METHOD.GET, params);
};

//综合概览-公共交通
export const getZongheGgjt = async (params) => {
  return request(BASE_URL + "/dataScreen/zonghe/ggjt", METHOD.GET, params);
};
//综合概览-行业治理
export const getZongheHyzl = async (params) => {
  return request(BASE_URL + "/dataScreen/zonghe/hyzl", METHOD.GET, params);
};

//综合概览-基础设施
export const getZongheJscs = async (params) => {
  return request(BASE_URL + "/dataScreen/zonghe/jscs", METHOD.GET, params);
};

//综合概览-基础设施-机场停机数
export const getPositionNum = async (params) => {
  return request(BASE_URL + "/dataScreen/airportInformation/positionNum", METHOD.GET, params);
};

//综合概览-西部陆海新通道
export const getZongheXblhxtd = async (params) => {
  return request(BASE_URL + "/dataScreen/zonghe/xblhxtd", METHOD.GET, params);
};

//综合概览-运输服务
export const getZongheYsfw = async (params) => {
  return request(BASE_URL + "/dataScreen/zonghe/ysfw", METHOD.GET, params);
};

//公路-服务荣耀
export const getGongluFwry = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/fwry", METHOD.GET, params);
};
//公路-监测预警
export const getGongluJcyj = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/jcyj", METHOD.GET, params);
};
//公路-路网综合
export const getGongluLwzh = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/lwzh", METHOD.GET, params);
};

//公路-公路规划
export const getGongluGlgh = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/glgh", METHOD.GET, params);
};

//公路-运行设备
export const getGongluYxsb = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/yxsb", METHOD.GET, params);
};


//公路-公路-公路养护
export const getGongluGljbz = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/gljbz", METHOD.GET, params);
};
//公路-公路-基础设施
export const getGongluYxss = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/yxss", METHOD.GET, params);
};

//公路-公路管养
export const getGongluGlgy = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/glgy", METHOD.GET, params);
};

//公路-公路建设
export const getGongluGljs = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/gljs", METHOD.GET, params);
};

//公路-车流量数据详情
export const getGongluTccll = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/tccll", METHOD.GET, params);
};
//公路-在建高速列表
export const getGongluTczjgslb = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/tczjgslb", METHOD.GET, params);
};

//公路-危险桥梁
export const getGongluTcwxql = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/tcwxql", METHOD.GET, params);
};

//公路-危险隧道
export const getGongluTcwxsd = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/tcwxsd", METHOD.GET, params);
};

//公路-出行服务 普通公路
export const getGlItemConstitute = async (params) => {
  return request(BASE_URL + "/dataScreen/hotConcern/glItemConstitute", METHOD.GET, params);
};
//公路-出行服务 高速
export const getCxfwData = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/cxfw", METHOD.GET, params);
};

//公路--公路运行
export const getGongluGlyn = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/glyn", METHOD.GET, params);
};

//公路-路网通行
export const getGonglulwtx = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/lwtx", METHOD.GET, params);
};

//公路--路网运行
export const getGonglulwyn = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/lwyn", METHOD.GET, params);
};
//公路--国省道事件
export const getGongluGsdsj = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/gsdsj", METHOD.GET, params);
};

//公路--高速公路事件
export const getGongluGsglsj = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/gsglsj", METHOD.GET, params);
};

//公路--收费站拥堵
export const getGongluSfzyd = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/sfzyd", METHOD.GET, params);
};

//公路--高速公路实时交通量
export const getGongluSsjt = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/gsglssjtl", METHOD.GET, params);
};

//公路-公路现状
export const getGongluGlxz = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/glxz", METHOD.GET, params);
};
//公路-救援
export const getGongluGsgljy = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/gsgljy", METHOD.GET, params);
};

//公路-国省道事件详情
export const getGongluGsdsjxq = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/gsdsjxq", METHOD.GET, params);
};

//公路-高速事件详情
export const getGongluGslsjxq = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/gsglsjxq", METHOD.GET, params);
};

//公路-高速公路建设项目投资计划表
export const getGongluTcjhb = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/tcjhb", METHOD.GET, params);
};

//公路-充电桩信息
export const getGongluTccdz = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/tccdz", METHOD.GET, params);
};

//公路-换电站信息统计
export const getGongluTchdz = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/tchdz", METHOD.GET, params);
};

//公路-换电站信息统计
export const getGongluTcjqz = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/tcjqz", METHOD.GET, params);
};

//公路-司机之家信息
export const getGongluTcsjzj = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/tcsjzj", METHOD.GET, params);
};

// 阳光救援获取token
export const getYgjytoken = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/ygjytoken", METHOD.GET, params);
};
// 基础设施信息
export const getBasicsFacilities = async (params) => {
  return request(BASE_URL + "/dataScreen/basicsFacilities/index", METHOD.GET, params);
};
// KPI信息
export const getKpiInfo = async (params) => {
  return request(BASE_URL + "/dataScreen/basicsFacilities/otherKpi", METHOD.GET, params);
};
// KPI点击弹窗的信息
export const getKpiClickInfo = async (params) => {
  return request(BASE_URL + "/dataScreen/basicsFacilities/otherKpiClick", METHOD.GET, params);
};
//水路-船舶
export const getShuiluCb = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/cb", METHOD.GET, params);
};

//水路-港口码头
export const getShuiluGkmt = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/gkmt", METHOD.GET, params);
};

//水路-航道
export const getShuiluHd = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/hd", METHOD.GET, params);
};
//水路-水路运输
export const getShuiluSlys = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/slys", METHOD.GET, params);
};
//水路-船舶检验
export const getShuiluCbjy = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/cbjy", METHOD.GET, params);
};
//水路-船舶报废
export const getShuiluCbbf = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/cbbf", METHOD.GET, params);
};

//水路-两江游当日实时客运量
export const liangjiangyoukyl = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/liangjiangyoukyl", METHOD.GET, params);
};

//水路-三峡游当日实时客运量
export const sanxiakyl = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/sanxiakyl", METHOD.GET, params);
};

//水路-运输(分时段)
export const getYs = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/ys", METHOD.GET, params);
};
//水路-资源(分时段)
export const getSlzy = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/slzy", METHOD.GET, params);
};
//水路货运运行信息(分时段)
export const getSlhyyxxx = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/slhyyxxx", METHOD.GET, params);
};
//水路-水路客运(分时段)
export const getSlky = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/slky", METHOD.GET, params);
};

//水路-在建航道整治工程项目 在建航道整治工程项目(分时段)
export const getSlzjhdzzgcxm = async (params) => {
  return request(BASE_URL + "/dataScreen/shuilu/slzjhdzzgcxm", METHOD.GET, params);
};

//运输-班线客运
export const getYsBxky = async (params) => {
  return request(BASE_URL + "/dataScreen/yunshu/bxky", METHOD.GET, params);
};

//运输-出租车
export const getYsCzc = async (params) => {
  return request(BASE_URL + "/dataScreen/yunshu/czc", METHOD.GET, params);
};
//运输-轨道
export const getYsGj = async (params) => {
  return request(BASE_URL + "/dataScreen/yunshu/gj", METHOD.GET, params);
};
//运输-预警
export const getYsYj = async (params) => {
  return request(BASE_URL + "/dataScreen/yunshu/yj", METHOD.GET, params);
};
//运输-货运
export const getYsHy = async (params) => {
  return request(BASE_URL + "/dataScreen/yunshu/hy", METHOD.GET, params);
};

//公路事件弹窗
export const getGlEventList = async (params) => {
  return request(BASE_URL + "/dataScreen/gonglu/eventList", METHOD.GET, params);
};

//p2公路管理
export const getRoadData = async (params) => {
  return request(BASE_URL + "/dataScreen/basicsFacilities/roadManagement", METHOD.GET, params);
};
//p2港行管理
export const getHarborShipP2 = async (params) => {
  return request(BASE_URL + "/dataScreen/basicsFacilities/harborShipP2", METHOD.GET, params);
};
//p2民航管理
export const getAviationP2 = async (params) => {
  return request(BASE_URL + "/dataScreen/basicsFacilities/aviationP2", METHOD.GET, params);
};
// //p2铁路管理
// export const getRoadData = async (params) => {
//   return request(BASE_URL + "/dataScreen/basicsFacilities/roadManagement", METHOD.GET, params);
// };