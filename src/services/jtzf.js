
import { request, METHOD } from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
// 交通数据
export const getJtzfList = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/latest', METHOD.GET, params)
}
// 质检列表数据
export const getQualityList = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/quality', METHOD.GET, params)
}
// 案件列表数据
export const getAnjianPage = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/anjianPage', METHOD.GET, params)
}
// 交通违法案件平均办案折线
export const getAnjianNumber = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/anjianNumber', METHOD.GET, params)
}
// 巡游出租汽车投诉分类与办结率
export const getXyczc = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/xyczc', METHOD.GET, params)
}
// 网约车派单分类与合规率
export const getWyczc = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/wyczc', METHOD.GET, params)
}
// 重点超限区域
export const getPointLines = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/pointLines', METHOD.GET, params)
}
// 案件办理
export const getCaseHandler = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/caseHandler', METHOD.GET, params)
}
//
export const getCheckWarning = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/checkWarning', METHOD.GET, params)
}
export const caseHandler = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/caseHandler', METHOD.GET, params)
}
export const checkWarning = async (params) => {
    return request(BASE_URL + '/dataScreen/trafficZf/checkWarning', METHOD.GET, params)
}




