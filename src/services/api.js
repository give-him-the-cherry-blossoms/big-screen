//跨域代理前缀
const BASE_URL = process.env.VUE_APP_URL
console.log('BASE_URL==>', BASE_URL)
module.exports = {
    //
    getRailTransitFlow:'/portal-api/a/dataScreen/getRailTransitFlowData',
    getVehicleDetection:'/portal-api/a/dataScreen/getVehicleDetectionData',
    getJCData:'/portal-api/a/dataScreen/getJiangBeiTabData',
    getJiangbeiAirport:'/portal-api/a/dataScreen/getJiangbeiAirportData',
    getSanxiaLiangjiang:'/portal-api/a/dataScreen/getSanxiaLiangjiangData',
    getMarketOverviews:'/portal-api/a/dataScreen/getMarketOverviewData',
    getApprovalEventCount:'/portal-api/a/dataScreen/getApprovalEventCountData',
    getPostalOperation:'/portal-api/a/dataScreen/getPostalOperationData',
    getPostalCurrent:'/portal-api/a/dataScreen/getPostalCurrentData',
    getRoadWayPassengerDatas:'/portal-api/a/dataScreen/getRoadWayPassengerData',
    ZHGLINFOleft:'/portal-api/a/dataScreen/getWaterWayPassengerData',
    ZHGLINFOS: `/portal-api/a/dataScreen/getInfrastructureCountData`,
    // 综合概览
    ZHGLINFO: `/portal-api/a/dataScreen/getDataByKeys`,
    //重点项目
    ZDXMLIST: `/portal-api/a/dataScreen/getImportantProjectData`,
    // 综合概览-当天高速车流量
    ZHGLDTGSCLL: `/portal-api/a/dataScreen/getHighWayFlowData`,
    // 综合概览-当前在网车辆
    ZHGLDQZWCL: `/portal-api/a/dataScreen/getNetWorkFlowData`,
    // 综合概览-出租车营收指标
    ZHGLCZC: `/portal-api/a/dataScreen/getTaxiRevenueData`,
    // 综合概览-公交客流量
    ZHGLGJKLL: `/portal-api/a/dataScreen/getBusPassengerFlowData`,
    // 综合概览-轨道客流量
    ZHGLGDKLL: `/portal-api/a/dataScreen/getRailTransitFlowData`,
    // 综合概览-雷达图--end
    ZHGLMBRWLD: `/portal-api/a/dataScreen/getTargetTaskData`,
    // 数字公路-公路规划(当年计划、五年规划、中长期规划)--start
    SZGLGLGH: `/portal-api/a/dataScreen/getHighWayPlanData`,
    //数字公路-公路建设-投资进度
    SZGLTZJD: `/portal-api/a/dataScreen/getConstructionSectionData`,
    //数字公路-当年重点工程项目
    SZGLZDGCXM: `/portal-api/a/dataScreen/getHighWayImportantProjectData`,
    //畅行高速-外省入渝车流量
    CXGSWSRYCLL: `/portal-api/a/dataScreen/getProvincialFlowData`,
    //畅行高速-在建高速项目
    CXGSZJGSXM: `/portal-api/a/dataScreen/getHighSpeedProjectData`,
    //数字铁路-当年重点工程项目
    DNZDGCXM: `/portal-api/a/dataScreen/getRailWayImportantProjectData`,
    //数字铁路-拟开工项目
    NKGXM: `/portal-api/a/dataScreen/getRailWayPlanProjectData`,
    //数字铁路-铁路外部安全环境(饼图和指标)
    WBAQHJ: `/portal-api/a/dataScreen/getRailWaySecurityMetricData`,
    //数字港航-水运建设(饼图和指标)
    SZGHSYJS: `/portal-api/a/dataScreen/getConstructionSectionData`,
    //数字港航-柱状图
    SZGHHDYH: `/portal-api/a/dataScreen/getWaterWayMileageData`,
    //数字港航-列表
    SZGHDNZDXM: `/portal-api/a/dataScreen/getWaterWayImportantProjectData`,
    //数字民航-民航建设
    SZMHMHJS: `/portal-api/a/dataScreen/getConstructionSectionData`,
    //数字民航-列表
    SZMHDNZDXM: `/portal-api/a/dataScreen/getAirWayImportantProjectData`,
    //数字邮政-邮政建设
    SZYZYZJS: `/portal-api/a/dataScreen/getConstructionSectionData`,
    //综合执法-行政处罚(指标和柱线图)
    ZHZFXZCF: `/portal-api/a/dataScreen/getPenaltyData`,
    //综合执法-行政强制(指标和柱线图)
    ZHZFXZQZ: `/portal-api/a/dataScreen/getCoercionData`,
    //综合执法-投诉处理(指标和柱线图)
    ZHZFTSCL: `/portal-api/a/dataScreen/getComplaintsData`,
    //审批信用-审批-高频事件办件Top10
    SPGPSJ: `/portal-api/a/dataScreen/getHighFrequencyPaperWorkTopData`,
    //审批信用-信用-经典案例(列表)
    XYJDAL: `/portal-api/a/dataScreen/getClassicCaseData`,
    //平安交通指数-月度事故信息(饼图和指标)
    YDSGXX: `/portal-api/a/dataScreen/getMonthlyAccidentData`,
    //平安交通指数-最新的有数据的月份
    YDSGM: `/portal-api/a/dataScreen/getMonthData`,
    //平安交通指数-月度事故同比情况(柱形图)
    YDSGTBQK: `/portal-api/a/dataScreen/getMonthlyAccidentYoyData`,
    //平安交通指数-年度事故数对比(柱状图)
    NDSGDB: `/portal-api/a/dataScreen/getYearAccidentData`,
    //平安交通指数-年度死亡人数
    NDSWRSDB: `/portal-api/a/dataScreen/getYearAccidentAndDeathData`,
    //平安交通指数-地区事故数据(列表)
    DQSGPM: `/portal-api/a/dataScreen/getAreaAccidentData`,
    //安全应急-平安交通指数-年度事故趋势(柱状图)
    NDSGQS: `/portal-api/a/dataScreen/getYearAccidentTrendData`,
    //运输服务-公共交通-轨道排名(列表)
    GDPM: `/portal-api/a/dataScreen/getRailRankingData`,
    //运输服务-公共交通-公交排名(列表)
    GJPM: `/portal-api/a/dataScreen/getBusRankingData`,
    //运输服务-公共交通-每日公交客流量-指标
    MRKLL: `/portal-api/a/dataScreen/getDailyPassengerFlowOtherData`,
    //审批信用-信用-从业企业、人员参评规模
    CYQYRYBAR: `/portal-api/a/dataScreen/getPracticeEnterpriseQuantityData`,
    //审批信用-信用-全市信用评价信息采集量(曲线图)
    QSXYPJXXCJT: `/portal-api/a/dataScreen/getCityCreditEvaluationData`,
    //审批信用-信用-动态评价和安全一票否决(列表)
    DTPJHAQ: `/portal-api/a/dataScreen/getDynamicEvaluationData`,
    //审批信用-信用-道路运输重点监管名单(列表)
    DLYSZDJGMD: `/portal-api/a/dataScreen/getFocusRegulationData`,
    //审批信用-信用-信用指标分领域统计(横向柱形图)
    XYZB: `/portal-api/a/dataScreen/getCreditTargetByFieldData`,
    //审批信用-信用-信用数据采集归集前五名(列表)
    XYSJCJ: `/portal-api/a/dataScreen/getCreditGatherTopData`,
    //审批信用-信用-从业企业、人员信用评价(列表)
    CYQYRYLIST: `/portal-api/a/dataScreen/getEpCreditRatingData`,
    //安全应急-渝运安-两客一危(指标)/portal-api/a/dataScreen/getHundredCartPitfallTopData
    LKYW: `/portal-api/a/dataScreen/getCbvData`,
    //安全应急-渝运安-区县百车隐患数前Top10(列表)
    QXBC: `/portal-api/a/dataScreen/getHundredCartPitfallTopData`,
    //安全应急-渝运安-风险隐患趋势(折线图和指标)
    FXYHQS: `/portal-api/a/dataScreen/getRiskHazardTrendData`,
    //安全应急-渝运安-车辆动态监督考核前3和后3
    CLDTJDKH: `/portal-api/a/dataScreen/getCarAssessData`,
    //安全应急-交通应急资源-水上救援分布(列表)
    SSJYFB: `/portal-api/a/dataScreen/getWaterRescueData`,
    //安全应急-交通应急资源-公路救援-公路阻断信息(列表)
    GLZDXX: `/portal-api/a/dataScreen/getHighWayBlockageData`,
    //运输服务-货运物流-危货道路运输信息(指标)
    WHDLYSXX: `/portal-api/a/dataScreen/getDangerousGoodsTransportData`,
    //安全应急-平安交通指数-年度事故数据信息(指标)
    NDSGSJXX: `/portal-api/a/dataScreen/getAnnualAccidentDeathData`,
    //畅行高速-高速运行-路网信息、当日管制、当天统计数据(指标)
    LWXX: `/portal-api/a/dataScreen/getHighSpeedCurrentData`,
    //审批信用-审批-政府服务工作(横向柱形图)
    ZFFWGZ: `/portal-api/a/dataScreen/getGovernmentWorkData`,
    //运输服务-公共交通-出租车单日运营情况(指标)
    CZCDR: `/portal-api/a/dataScreen/getTaxiOperationData`,
    //综合执法-行政检查(指标和饼图)
    XZJC: `/portal-api/a/dataScreen/getInspectionData`,
    //公路治超
    GLZC: `/portal-api/a/dataScreen/getControlOverData`,
    //审批信用-审批-全市办件总量(饼图)
    QSBJQK: `/portal-api/a/dataScreen/getCityPaperWorkTotalData`,
    //处室视窗start
    //建管处-首页-投资规模
    JGCTZGM: `/portal-api/a/officeDataScreen/jg/getInvestmentScaleData`,
    //建管处-首页-开工项目
    JGCKGXM: `/portal-api/a/officeDataScreen/jg/getStartProjectMileageData`,
    //建管处-首页-通车项目
    JGCTCXM: `/portal-api/a/officeDataScreen/jg/getOpenProjectMileageData`,
    //建管处-首页-建设里程
    JGCJSLC: `/portal-api/a/officeDataScreen/jg/getConstructionMileageData`,
    //建管处-首页-运营里程
    JGCYYLC: `/portal-api/a/officeDataScreen/jg/getOperatingMileageData`,
    //管养处-首页-流量信息
    GYCDRRKLL: `/portal-api/a/officeDataScreen/gy/getHighwayEntranceFlowData`,
    //管养处-首页-突发事件
    GYCTFSJ: `/portal-api/a/officeDataScreen/gy/getEmergenciesData`,
    //管养处-首页-管制情况
    GYCGZQK: `/portal-api/a/officeDataScreen/gy/getRegulatoryData`,
    //管养处-运行-交通指数
    GYCJTZS:`/portal-api/a/officeDataScreen/gy/getTrafficIndexData`,
    //管养处-收费-营业情况
    GYCYYQK: `/portal-api/a/officeDataScreen/sf/getTollStationRevenueData`,
    //管养处-收费-经营单位(柱形图)
    GYCSFJYDW: `/portal-api/a/officeDataScreen/gy/getRevenueSituation`,
    //科技处视窗 应用规模-运输(指标)
    KJCSSCYYGMYS: `/portal-api/a/officeDataScreen/kj/getTransportationScale`,
    getTransport: `/portal-api/a/officeDataScreen/gh/getTransportPassenger`,
    // 管养处-收费-收费单位公路排名(列表)
    getHighWayTollRankTop: `/portal-api/a/officeDataScreen/gy/getHighWayTollRankTopData`,
    //根据模块关键字查询
    MKGJZ: `/portal-api/a/officeDataScreen/getDataByKeys`,
    //数字民航-民航现状-机场体系-江北机场-月度起飞航班数、月度登机人数(柱形图)
    JiangBei: `/portal-api/a/dataScreen/getJiangBeiTabChartData`,
    //轨道客流量
    RailTransit: `/portal-api/a/dataScreen/getRailTransitFlowData`,


}
