//综合概览请求地址
import {
  ZHGLINFO,
  ZDXMLIST,
  ZHGLDTGSCLL,
  ZHGLDQZWCL,
  ZHGLCZC,
  ZHGLGJKLL,
  ZHGLGDKLL,
  ZHGLMBRWLD,
  ZHGLINFOleft,
  ZHGLINFOS,
  getRoadWayPassengerDatas,
  getPostalCurrentDatas,
  getPostalCurrent,
  getPostalOperation, getApprovalEventCount, getMarketOverviews,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
import {getJCData, getJiangbeiAirport, getRailTransitFlow, getSanxiaLiangjiang, getVehicleDetection} from "./api";
const BASE_URL = process.env.VUE_APP_URL
//综合概览信息
export async function getzhglinfo(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+ZHGLINFO, METHOD.GET, params)
}
export async function getMarketOverviewData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getMarketOverviews, METHOD.GET, params)
}
export async function getApprovalEventCountData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getApprovalEventCount, METHOD.GET, params)
}
export async function getPostalOperationData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getPostalOperation, METHOD.GET, params)
}
export async function getPostalCurrentData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getPostalCurrent, METHOD.GET, params)
}
export async function getzhglinfos(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+ZHGLINFOleft, METHOD.GET, params)
}
export async function getSanxiaLiangjiangData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getSanxiaLiangjiang, METHOD.GET, params)
}
export async function getJiangbeiAirportData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getJiangbeiAirport, METHOD.GET, params)
}
export async function getRailTransitFlowData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getRailTransitFlow, METHOD.GET, params)
}
export async function getJiangBeiTabData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getJCData, METHOD.GET, params)
}
export async function getVehicleDetectionData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getVehicleDetection, METHOD.GET, params)
}
export async function getRoadWayPassengerData(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+getRoadWayPassengerDatas, METHOD.GET, params)
}
export async function getzhglinfoss(params) {
  console.log(ZHGLINFO)
  return request(BASE_URL+ZHGLINFOS, METHOD.GET, params)
}
//表格信息
export async function getTableList() {
  return request(BASE_URL+ZDXMLIST, METHOD.GET)
}
//综合概览-当天高速车流量
export async function getZhglGscll() {
  return request(BASE_URL+ZHGLDTGSCLL, METHOD.GET)
}
//综合概览-高速在网车流量
export async function getZhglGszwcll() {
  return request(BASE_URL+ZHGLDQZWCL, METHOD.GET)
}
//综合概览-出租车营收指标
export async function getZhglCzc() {
  return request(BASE_URL+ZHGLCZC, METHOD.GET)
}
//综合概览-公交客流量
export async function getZhglGjkll() {
  return request(BASE_URL+ZHGLGJKLL, METHOD.GET)
}
//综合概览-轨道客流量
export async function getZhglGdkll() {
  return request(BASE_URL+ZHGLGDKLL, METHOD.GET)
}
//综合概览-雷达图
export async function getZhglLd() {
  return request(BASE_URL+ZHGLMBRWLD, METHOD.GET)
}
export default {
  getzhglinfo,
  getVehicleDetectionData,
  getJiangBeiTabData,
  getSanxiaLiangjiangData,
  getJiangbeiAirportData,
  getzhglinfos,
  getTableList,
  getZhglLd,
  getZhglGscll,
  getZhglGszwcll,
  getZhglCzc,
  getZhglGjkll,
  getZhglGdkll,
}
