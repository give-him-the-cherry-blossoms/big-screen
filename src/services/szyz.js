//数字邮政请求地址
import {
  SZYZYZJS,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//数字民航-列表
export async function getszyzyzjs(params) {
  return request(BASE_URL+SZYZYZJS+'?type=5', METHOD.GET, params)
}
export default {
  getszyzyzjs
}
