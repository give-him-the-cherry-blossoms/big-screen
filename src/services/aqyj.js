
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
// 电子运单
export const getDzydList=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/electronicTrans', METHOD.GET, params)
}
// 高风险企业数
export const getDanderousCompanyList=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/ownerGw', METHOD.GET, params)
}
// 中间模块
export const getCenterInfo=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/centerTitle', METHOD.GET, params)
}
// 道路危货运输“五必查”-区县
export const getWbcCounty=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/county', METHOD.GET, params)
}
// 道路危货运输“五必查”-详情
export const getWbcCountyDetail=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/countyCheck', METHOD.GET, params)
}
// 年度安全隐患
export const getTroubleYear=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/companyTrouble', METHOD.GET, params)
}
// 年度安全隐患-弹窗
export const getTroubleYearDialog=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/companyTroubleView', METHOD.GET, params)
}
// 道路危货运输高风险企业列表-弹窗
export const getTroubleCompanyDialog=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/ownerGwView', METHOD.GET, params)
}
// 两客一危车辆超速驾驶平均量
export const getdoubleLuTrouble=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/doubleLuTrouble', METHOD.GET, params)
}
// 道路危货运输隐患
export const getdlTrouble=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/dlTrouble', METHOD.GET, params)
}
// 安全风险评价
export const getSecurityRisk=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/securityRisk', METHOD.GET, params)
}
// 安全风险评价
export const getTroubleSecure=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/troubleSecure', METHOD.GET, params)
}
// 进场,装载,卸载和运输
export const getUnloadAndTransport=async (params) => {
    return request(BASE_URL+'/dataScreen/waybill/unloadAndTransport', METHOD.GET, params)
}

