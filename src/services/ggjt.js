//公共交通请求地址
import {
  GDPM,
  GJPM,
  MRKLL,
  CZCDR,
  RailTransit,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//运输服务-公共交通-轨道排名
export async function getgdpm(params) {
  return request(BASE_URL+GDPM, METHOD.GET, params)
}
//运输服务-公共交通-公交排名
export async function getgjpm(params) {
  return request(BASE_URL+GJPM, METHOD.GET, params)
}
//运输服务-每日客流量
export async function getmrkll(params) {
  return request(BASE_URL+MRKLL, METHOD.GET, params)
}
//运输服务-公共交通-出租车单日运营情况(指标)
export async function getczcdr(params) {
  return request(BASE_URL+CZCDR, METHOD.GET, params)
}
export async function getRailTransit(params) {
  return request(BASE_URL+RailTransit, METHOD.GET, params)
}

export default {
  getgdpm,
  getgjpm,
  getmrkll,
  getczcdr,
  getRailTransit,
}
