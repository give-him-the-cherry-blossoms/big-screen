//渝运安请求地址
import {
  LKYW,
  QXBC,
  FXYHQS,
  CLDTJDKH,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//
export async function getlkyw(params) {
  return request(BASE_URL+LKYW, METHOD.GET, params)
}
//区县百车隐患数前10
export async function getqxbc(params) {
  return request(BASE_URL+QXBC, METHOD.GET, params)
}
//风险隐患趋势
export async function getfxyhqs(params) {
  return request(BASE_URL+FXYHQS, METHOD.GET, params)
}
//安全应急-渝运安-车辆动态监督考核前3和后3
export async function getcldtjdkh(params) {
  return request(BASE_URL+CLDTJDKH, METHOD.GET, params)
}
export default {
  getlkyw,
  getqxbc,
  getfxyhqs,
  getcldtjdkh,
}
