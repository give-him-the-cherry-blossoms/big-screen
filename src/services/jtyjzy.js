//交通应急资源请求地址
import {
  SSJYFB,
  GLZDXX,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//水上救援分布
export async function getssjyfb(params) {
  return request(BASE_URL+SSJYFB, METHOD.GET, params)
}
//公路阻断信息
export async function getqglzdxx(params) {
  return request(BASE_URL+GLZDXX, METHOD.GET, params)
}
export default {
  getssjyfb,
  getqglzdxx,
}
