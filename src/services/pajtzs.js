//综合执法请求地址
import {
  YDSGXX,
    YDSGM,
  YDSGTBQK,
  NDSGDB,
  NDSWRSDB,
  DQSGPM,
  NDSGQS,
  NDSGSJXX,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//平安交通指数-月度事故信息
export async function getydsgxx(params) {
  return request(BASE_URL+YDSGXX, METHOD.GET, params)
}
//平安交通指数-获取最新的有数据的月份
export async function getNewMonth() {
  return request(BASE_URL+YDSGM, METHOD.GET)
}
//平安交通指数-月度事故同比情况
export async function getydsgtbqk(params) {
  return request(BASE_URL+YDSGTBQK, METHOD.GET, params)
}
//平安交通指数-年度事故对比
export async function getndsgdb(params) {
  return request(BASE_URL+NDSGDB, METHOD.GET, params)
}
//平安交通指数-年度死亡人数对比
export async function getndswrsdb(params) {
  return request(BASE_URL+NDSWRSDB, METHOD.GET, params)
}
//平安交通指数-地区事故数据(列表)
export async function getdqsgpm(params) {
  return request(BASE_URL+DQSGPM, METHOD.GET, params)
}
//平安交通指数-年度事故趋势
export async function getndsgqs(params) {
  return request(BASE_URL+NDSGQS, METHOD.GET, params)
}
//安全应急-平安交通指数-年度事故数据信息(指标)
export async function getndsgsjxx(params) {
  return request(BASE_URL+NDSGSJXX, METHOD.GET, params)
}
export default {
  getydsgxx,
  getydsgtbqk,
  getndsgdb,
  getndswrsdb,
  getdqsgpm,
  getndsgqs,
  getndsgsjxx,
}
