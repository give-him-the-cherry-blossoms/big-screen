//建管处
import {
    JGCTZGM,
    JGCKGXM,
    JGCTCXM,
    JGCJSLC,
    JGCYYLC,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'

const BASE_URL = process.env.VUE_APP_URL

//投资规模
export async function getjgctzgm(params) {
    return request(BASE_URL + JGCTZGM, METHOD.GET, params)
}

export async function getjgckgxm(params) {
    return request(BASE_URL + JGCKGXM, METHOD.GET, params)
}

export async function getjgctcxm(params) {
    return request(BASE_URL + JGCTCXM, METHOD.GET, params)
}
export async function getjgcjslc(params) {
    return request(BASE_URL + JGCJSLC, METHOD.GET, params)
}
export async function getjgcyylc(params) {
    return request(BASE_URL + JGCYYLC, METHOD.GET, params)
}
