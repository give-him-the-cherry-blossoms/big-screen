import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
// 电子运单
export const getFreightLogistics=async (params) => {
    return request(BASE_URL+'/dataScreen/tabTransport/freightLogistics', METHOD.GET, params)
}