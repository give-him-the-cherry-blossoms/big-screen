//数字铁路请求地址
import {
  DNZDGCXM,
  NKGXM,
  WBAQHJ,
  SZGLTZJD
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//数字铁路-当年重点工程项目
export async function getdnzdgcxm(params) {
  return request(BASE_URL+DNZDGCXM, METHOD.GET, params)
}
//数字铁路-拟开工项目
export async function getnkgxm(params) {
  return request(BASE_URL+NKGXM, METHOD.GET, params)
}
//数字铁路-铁路建设
export async function getjcjs(params) {
  return request(BASE_URL+SZGLTZJD+'?type=2', METHOD.GET, params)
}
//数字铁路-外部安全环境
export async function getwbaqhj(params) {
  return request(BASE_URL+WBAQHJ, METHOD.GET, params)
}
export default {
  getdnzdgcxm,
  getnkgxm,
  getwbaqhj,
  getjcjs,
}
