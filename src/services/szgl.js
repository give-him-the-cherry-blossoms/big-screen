//数字公路请求地址
import {
  SZGLGLGH,
  SZGLTZJD,
  SZGLZDGCXM
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//公路规划
export async function getszglglgh(params) {
  return request(BASE_URL+SZGLGLGH, METHOD.GET, params)
}
//公路建设-投资进度
export async function getszgltzjd(params) {
  return request(BASE_URL+SZGLTZJD+'?type=1', METHOD.GET, params)
}
//公路建设-当年重点项目
export async function getszgldnzdxm(params) {
  return request(BASE_URL+SZGLZDGCXM, METHOD.GET, params)
}
export default {
  getszglglgh,
  getszgltzjd,
  getszgldnzdxm
}
