import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
export const retentionProcess=async (params) => {
    return request(BASE_URL+'/dataScreen/railWay/retentionProcess', METHOD.GET, params)
}

export const trainDelay=async (params) => {
    return request(BASE_URL+'/dataScreen/railWay/trainDelay', METHOD.GET, params)
}

export const passageDetention=async (params) => {
    return request(BASE_URL+'/dataScreen/airportInformation/passageDetention', METHOD.GET, params)
}
export const retentionProcessList=async (params) => {
    return request(BASE_URL+'/dataScreen/airportInformation/retentionProcess', METHOD.GET, params)
}

export const dmjhbywhbjPage=async (params) => {
    return request(BASE_URL+'/dataScreen/airportInformation/dmjhbywhbjPage', METHOD.GET, params)
}
export const dmjhbywhbjDetail=async (params) => {
    return request(BASE_URL+'/dataScreen/airportInformation/dmjhbywhbjDetail', METHOD.GET, params)
}
// 营业场所弹窗
export const businessPopUp=async (params) => {
    return request(BASE_URL+'/dataScreen/postalData/businessPopUp', METHOD.GET, params)
}
// 快递企业弹窗
export const enterprisePopUp=async (params) => {
    return request(BASE_URL+'/dataScreen/postalData/enterprisePopUp', METHOD.GET, params)
}

