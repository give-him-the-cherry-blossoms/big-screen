//综合执法请求地址
import {
  SPGPSJ,
  XYJDAL,
  ZHZFTSCL,
  CYQYRYBAR,
  QSXYPJXXCJT,
  DTPJHAQ,
  DLYSZDJGMD,
  XYZB,
  XYSJCJ,
  CYQYRYLIST,
  ZFFWGZ,
  QSBJQK,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//审批信用-高频事件top
export async function getspgpsj(params) {
  return request(BASE_URL+SPGPSJ, METHOD.GET, params)
}
//审批信用-信用-经典案例
export async function getxyjdal(params) {
  return request(BASE_URL+XYJDAL, METHOD.GET, params)
}
//综合执法-行政强制
export async function gettscl(params) {
  return request(BASE_URL+ZHZFTSCL, METHOD.GET, params)
}
//审批信用-信用-从业企业、人员参评规模
export async function getcyqyrybar(params) {
  return request(BASE_URL+CYQYRYBAR, METHOD.GET, params)
}
//审批信用-信用-动态评价和安全一票否决(列表)
export async function getdtpjhaq(params) {
  return request(BASE_URL+DTPJHAQ, METHOD.GET, params)
}
//审批信用-信用-道路运输重点监管名单(列表)
export async function getdlyszdjgmd(params) {
  return request(BASE_URL+DLYSZDJGMD, METHOD.GET, params)
}
//审批信用-信用-信用指标分领域统计(横向柱形图)
export async function getxyzb(params) {
  return request(BASE_URL+XYZB, METHOD.GET, params)
}
//审批信用-信用-全市信用评价信息采集量(曲线图)
export async function getqsxypjxxcjt(params) {
  return request(BASE_URL+QSXYPJXXCJT, METHOD.GET, params)
}
//审批信用-信用-信用数据采集归集前五名(列表)
export async function getxysjcj(params) {
  return request(BASE_URL+XYSJCJ, METHOD.GET, params)
}
//审批信用-信用-信用数据采集归集前五名(列表)
export async function getcyqyrylist(params) {
  return request(BASE_URL+CYQYRYLIST, METHOD.GET, params)
}
//审批信用-审批-政府服务工作
export async function getzffwgz(params) {
  return request(BASE_URL+ZFFWGZ, METHOD.GET, params)
}
//审批信用-审批-全市办件总量(饼图)
export async function getqsbjqk(params) {
  return request(BASE_URL+QSBJQK, METHOD.GET, params)
}
export default {
  getspgpsj,
  getxyjdal,
  gettscl,
  getcyqyrybar,
  getdtpjhaq,
  getdlyszdjgmd,
  getxyzb,
  getqsxypjxxcjt,
  getxysjcj,
  getcyqyrylist,
  getzffwgz,
  getqsbjqk,
}
