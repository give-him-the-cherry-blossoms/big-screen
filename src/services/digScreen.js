
import {request, METHOD, requestJson} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
export const getMonitoringList=async (params) => {
    return request(BASE_URL+'/dataScreen/monitoring/list', METHOD.GET, params)
}
// 在建高速项目
export const getItemRatio=async (params) => {
    return request(BASE_URL+'/dataScreen/hotConcern/itemRatio', METHOD.GET, params)
}

// 今日网络舆情量
export const getNetworkOpinionList=async (params) => {
    return request(BASE_URL+'/dataScreen/monitoring/itemCount', METHOD.GET, params)
}


export const getHotConcernItemConstitute=async (params) => {
    return request(BASE_URL+'/dataScreen/hotConcern/itemConstitute', METHOD.GET, params)
}
//投诉情况
export const getComplaint=async (params) => {
    return request(BASE_URL+'/dataScreen/hotConcern/complaint', METHOD.GET, params)
}
//咨询情况
export const getconsult=async (params) => {
    return request(BASE_URL+'/dataScreen/hotConcern/consult', METHOD.GET, params)
}
//监督评价-数据质量排名
export const unReport=async (params) => {
    return request(BASE_URL+'/dataScreen/supervisor/unReport', METHOD.GET, params)
}
// 各模块当日累计数据
export const geteveryModule=async (params) => {
    return request(BASE_URL+'/dataScreen/hotConcern/everyModule', METHOD.GET, params)
}
//关注事项数量今日趋势
export const getfollowEvent=async (params) => {
    return request(BASE_URL+'/dataScreen/hotConcern/followEvent', METHOD.GET, params)
}
// 今日热点事项
export const gethotConcernCurrent=async (params) => {
    return request(BASE_URL+'/dataScreen/hotConcern/hotConcernCurrent', METHOD.GET, params)
}
// 事件处置-左侧接口
export const getEventsItems=async (params) => {
    return request(BASE_URL+'/dataScreen/events/items', METHOD.GET, params)
}
// 事件处置-左侧接口
export const incidentNumber=async (params) => {
    return request(BASE_URL+'/dataScreen/events/incidentNumber', METHOD.GET, params)
}
// 事件处置-右侧接口
export const getEventsList=async (params) => {
    return request(BASE_URL+'/dataScreen/events/list', METHOD.GET, params)
}
// 交通执法模块数据
export const getTrafficModel=async (params) => {
    return request(BASE_URL+'/dataScreen/perception/trafficModel', METHOD.GET, params)
}
// // 舆情事项弹窗列表
export const getItemCheck=async (params) => {
    return request(BASE_URL+`/dataScreen/monitoring/itemCheck`, METHOD.GET,params)
}
// // 关注事项办理超期列表
export const enentOvertimeList=async (params) => {
    return request(BASE_URL+`/dataScreen/hotConcern/enentOvertimeList`, METHOD.GET,params)
}
//关注事项办理超期列表饼图
export const enentOvertimeListPie=async (params) => {
    return request(BASE_URL+`/dataScreen/hotConcern/enentOvertimeListPie`, METHOD.GET,params)
}
//关注事项办理差评列表
export const enentEvaluateList=async (params) => {
    return request(BASE_URL+`/dataScreen/hotConcern/enentEvaluateList`, METHOD.GET,params)
}
// 投诉情况详细列表
export const complaintList=async (params) => {
    return request(BASE_URL+`/dataScreen/hotConcern/complaintList`, METHOD.GET,params)
}
//咨询情况详细列表
export const consultList=async (params) => {
    return request(BASE_URL+`/dataScreen/hotConcern/consultList`, METHOD.GET,params)
}
// 各模块当日累计数据详细列表
export const everyModuleList=async (params) => {
    return request(BASE_URL+`/dataScreen/hotConcern/everyModuleList`, METHOD.GET,params)
}
//事项构成详细列表
export const itemConstituteList=async (params) => {
    return request(BASE_URL+`/dataScreen/hotConcern/itemConstituteList`, METHOD.GET,params)
}
// 舆情右侧弹窗详情
export const monitoringDetail=async (params) => {
    return request(BASE_URL+`/dataScreen/monitoring/detail`, METHOD.GET,params)
}
// 低效率工单详情
export const dxvDetail=async (params) => {
    return request(BASE_URL+'/dataScreen/citizenOpinion/getOrderById', METHOD.GET, params)
}
// 事件处置-弹窗列表
export const windowList=async (params) => {
    return request(BASE_URL+`/dataScreen/events/windowList`, METHOD.GET,params)
}
// 收费站拥堵
export const sfzyd=async (params) => {
    return request(BASE_URL+`/dataScreen/gonglu/sfzyd`, METHOD.GET,params)
}
// 国省道事件
export const gsdsj=async (params) => {
    return request(BASE_URL+`/dataScreen/gonglu/gsdsj`, METHOD.GET,params)
}

// 高速公路事件
export const gsglsj=async (params) => {
    return request(BASE_URL+`/dataScreen/gonglu/gsglsj`, METHOD.GET,params)
}
// 船舶报废
export const cbbf=async (params) => {
    return request(BASE_URL+`/dataScreen/shuilu/cbbf`, METHOD.GET,params)
}
// 船舶检验
export const cbjy=async (params) => {
    return request(BASE_URL+`/dataScreen/shuilu/cb`, METHOD.GET,params)
}
// 轨道正点率
export const gd=async (params) => {
    return request(BASE_URL+`/dataScreen/yunshu/gd`, METHOD.GET,params)
}
//运输-预警
export const yj=async (params) => {
    return request(BASE_URL+`/dataScreen/yunshu/yj`, METHOD.GET,params)
}
// 关注事项数量今日趋势详细列表
export const followEventList=async (params) => {
    return request(BASE_URL+`/dataScreen/hotConcern/followEventList`, METHOD.GET,params)
}
// 事件详情弹窗
export const eventDetails=async (params) => {
    return request(BASE_URL+`/dataScreen/events/eventDetails`, METHOD.GET,params)
}
export const urbanMovementDetails=async (params) => {
    return request(BASE_URL+`/dataScreen/urbanMovement/detail`, METHOD.GET,params)
}

// 列表通用详情
export const commonDetail=async (params) => {
    return request(BASE_URL+`/dataScreen/hotConcern/detail`, METHOD.GET,params)
}


export const centerTitle=async (params) => {
    return request(BASE_URL+`/dataScreen/waybill/centerTitle`, METHOD.GET,params)
}
export const passageDetention=async (params) => {
    return request(BASE_URL+`/dataScreen/airportInformation/passageDetention`, METHOD.GET,params)
}
export const dmjhbywhbjPage=async (params) => {
    return request(BASE_URL+`/dataScreen/airportInformation/dmjhbywhbjPage`, METHOD.GET,params)
}
// 行政审批
export const administrative=async (params) => {
    return request(BASE_URL+`/dataScreen/supervisor/administrative`, METHOD.GET,params)
}
//信用
export const credit=async (params) => {
    return request(BASE_URL+`/dataScreen/supervisor/credit`, METHOD.GET,params)
}
export const baseMeasure=async (params) => {
    return request(BASE_URL+`/dataScreen/perception/baseMeasure`, METHOD.GET,params)
}
export const transportService=async (params) => {
    return request(BASE_URL+`/dataScreen/perception/transportService`, METHOD.GET,params)
}
export const examine=async (params) => {
    return request(BASE_URL+`/dataScreen/perception/examine`, METHOD.GET,params)
}

export const baseInformation=async (params) => {
    return request(BASE_URL+`/dataScreen/creditApproval/baseInformation`, METHOD.GET,params)
}
export const trafficEngineering=async (params) => {
    return request(BASE_URL+`/dataScreen/creditApproval/trafficEngineering`, METHOD.GET,params)
}
//民航
export const information=async (params) => {
    return request(BASE_URL+`/dataScreen/airportInformation/information`, METHOD.GET,params)
}
// 铁路
export const basicRailway=async (params) => {
    return request(BASE_URL+`/dataScreen/railWay/basicRailway`, METHOD.GET,params)
}
// 邮政
export const businessPremises=async (params) => {
    return request(BASE_URL+`/dataScreen/postalData/businessPremises`, METHOD.GET,params)
}
export const expressEnterprise=async (params) => {
    return request(BASE_URL+`/dataScreen/postalData/expressEnterprise`, METHOD.GET,params)
}
// 弹窗
export const mileagePopUp=async (params) => {
    return request(BASE_URL+`/dataScreen/railWay/mileagePopUp`, METHOD.GET,params)
}

export const gstk=async (params) => {
    return request(BASE_URL+`/dataScreen/tabTransport/gstk`, METHOD.GET,params)
}

export const basicPostal=async (params) => {
    return request(BASE_URL+`/dataScreen/postalData/basicPostal`, METHOD.GET,params)
}
export const infrastructure=async (params) => {
    return request(BASE_URL+`/dataScreen/transportationCore/infrastructure`, METHOD.GET,params)
}
// 总驾驶舱
export const mainCockpit=async (params) => {
    return request(BASE_URL+`/dataScreen/mainCockpit/infrastructure`, METHOD.GET,params)
}
export const trafficMainKPI=async (params) => {
    return request(BASE_URL+`/dataScreen/mainCockpit/trafficMainKPI`, METHOD.GET,params)
}
export const levelCount=async (params) => {
    return request(BASE_URL+`/dataScreen/events/levelCount`, METHOD.GET,params)
}
export const urbanMovementList=async (params) => {
    return request(BASE_URL+`/dataScreen/urbanMovement/list`, METHOD.GET,params)
}

export const warningNew=async (params) => {
    return request(BASE_URL+`/dataScreen/perception/warningNew`, METHOD.GET,params)
}
export const statusCount=async (params) => {
    return request(BASE_URL+'/dataScreen/urbanMovement/statusCount', METHOD.GET, params)
}

export const urbanMovementLevelCount=async (params) => {
    return request(BASE_URL+`/dataScreen/urbanMovement/levelCount`, METHOD.GET,params)
}
// KPI
export const trafficMainKPIClick=async (params) => {
    return request(BASE_URL+`/dataScreen/mainCockpit/trafficMainKPIClick`, METHOD.GET,params)
}
export const ssmContact=async (params) => {
    return request(BASE_URL+`/dataScreen/ssmContact`, METHOD.GET,params)
}
export const ejectment=async (params) => {
    return request(BASE_URL+`/dataScreen/ssmContact/ejectment`, METHOD.GET,params)
}
export const ssmContactPost=async (params) => {
    return requestJson(BASE_URL+`/dataScreen/ssmContact`, METHOD.POST,params)
}

export const detailNew=async (params) => {
    return request(BASE_URL+`/dataScreen/urbanMovement/detailNew`, METHOD.GET,params)
}
export const eventInformation=async (params) => {
    return request(BASE_URL+`/dataScreen/events/eventInformation`, METHOD.GET,params)
}

export const sendSsmRecord=async (params) => {
    return requestJson(BASE_URL+`/dataScreen/ssmRecord/send`, METHOD.POST,params)
}

export const shippingIntelligenceTube=async (params) => {
    return request(BASE_URL+`/dataScreen/basicsFacilities/shippingIntelligenceTube`, METHOD.GET,params)
}
export const aviationConstructP3=async (params) => {
    return request(BASE_URL+`/dataScreen/basicsFacilities/aviationConstructP3`, METHOD.GET,params)
}
export const highSpeedTravel=async (params) => {
    return request(BASE_URL+`/dataScreen/basicsFacilities/highSpeedTravel`, METHOD.GET,params)
}

export const lcjz=async (params) => {
    return request(BASE_URL+`/dataScreen/basicsFacilities/highroad/lcjz`, METHOD.GET,params)
}
export const construct=async (params) => {
    return request(BASE_URL+`/dataScreen/basicsFacilities/construct`, METHOD.GET,params)
}
export const maintain=async (params) => {
    return request(BASE_URL+`/dataScreen/basicsFacilities/maintain`, METHOD.GET,params)
}
export const portIntelligentControl=async (params) => {
    return request(BASE_URL+`/dataScreen/basicsFacilities/portIntelligentControl`, METHOD.GET,params)
}
export const shipWiseGovernance=async (params) => {
    return request(BASE_URL+`/dataScreen/basicsFacilities/shipWiseGovernance`, METHOD.GET,params)
}
export const lcjzReal=async (params) => {
    return request(BASE_URL+`/dataScreen/basicsFacilities/highroad/lcjzReal`, METHOD.GET,params)
}






