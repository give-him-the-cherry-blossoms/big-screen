//畅行高速请求地址
import {
  CXGSZJGSXM,
  CXGSWSRYCLL,
  LWXX,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//外省入渝车流量
export async function getcxgswsrycll(params) {
  return request(BASE_URL+CXGSWSRYCLL, METHOD.GET, params)
}
//在建高速项目
export async function getcxgszjgsxm(params) {
  return request(BASE_URL+CXGSZJGSXM, METHOD.GET, params)
}
//路网信息
export async function getlwxx(params) {
  return request(BASE_URL+LWXX, METHOD.GET, params)
}
export default {
  getcxgszjgsxm,
  getcxgswsrycll,
  getlwxx,
}
