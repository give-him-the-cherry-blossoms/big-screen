//货运物流请求地址
import {
  WHDLYSXX,
} from '@/services/api'
import {request, METHOD} from '@/utils/request'
const BASE_URL = process.env.VUE_APP_URL
//运输服务-货运物流-危货道路运输信息(指标)
export async function getwhdlysxx(params) {
  return request(BASE_URL+WHDLYSXX, METHOD.GET, params)
}
export default {
  getwhdlysxx,
}
