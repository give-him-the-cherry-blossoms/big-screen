//基础设施
const list = [{
		from: '数字政务',
		to: '高速畅行',
	},
	{
		from: '高速畅行',
		to: '高速公路一键救援',
	},
	{
		from: '高速公路一键救援',
		to: '生产生活服务,设施运行,应急动员',
	},
	{
		from: '数字政务',
		to: '数字邮管',
	},
	{
		from: '数字政务',
		to: '路长集治',
	},

	{
		from: '数字政务',
		to: '数字港航',
	},

	{
		from: '数字港航',
		to: '港口智控(谋划中)',
	},
	{
		from: '数字港航',
		to: '航运智管(谋划中)',
	},
	{
		from: '数字港航',
		to: '船舶智治(谋划中)',
	},
	{
		from: '数字港航',
		to: '航道智养(谋划中)',
	},
	{
		from: '港口智控(谋划中)',
		to: '设施运行,应急动员,生产生活服务',
	},

	{
		from: '航运智管(谋划中)',
		to: '设施运行,应急动员,生产生活服务',
	},
	{
		from: '船舶智治(谋划中)',
		to: '设施运行,应急动员,生产生活服务',
	},
	{
		from: '航道智养(谋划中)',
		to: '设施运行,应急动员,生产生活服务',
	},
]
const newList = [];
list.forEach(item => {
	if (item.to.indexOf(',') !== -1) {
		item.to.split(",").forEach(to => {
			newList.push({
				from: item.from,
				to: to
			})
		})
	} else {
		newList.push({
			...item
		})
	}
})
export default newList;