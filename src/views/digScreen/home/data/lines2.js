//运输服务
const list = [{
		from: '数字社会',
		to: '渝你同行',
	},
	{
		from: '渝你同行',
		to: '渝悦修车',
	},
	{
		from: '渝你同行',
		to: '一码享行',
	},
	{
		from: '渝你同行',
		to: '渝悦学车',
	},
	{
		from: '渝悦修车',
		to: '生产生活服务',
	},
	{
		from: '一码享行',
		to: '生产生活服务',
	},
	{
		from: '渝悦学车',
		to: '生产生活服务',
	},
	{
		from: '数字社会',
		to: '数智货运',
	},
]
const newList = [];
list.forEach(item => {
	if (item.to.indexOf(',') !== -1) {
		item.to.split(",").forEach(to => {
			newList.push({
				from: item.from,
				to: to
			})
		})
	} else {
		newList.push({
			...item
		})
	}
})
export default newList;