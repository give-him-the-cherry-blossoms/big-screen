import line1 from './lines1'
import line2 from './lines2'
import line3 from './lines3'

const newList = []
newList.push(...line1)
newList.push(...line2)
newList.push(...line3)

export default newList