//行业智治
const list = [{
		from: '数字政务',
		to: '渝运安',
	},
	{
		from: '渝运安',
		to: '普货运输风险监管',
	},
	{
		from: '渝运安',
		to: '货车超限源头监管',
	},
	{
		from: '渝运安',
		to: '货车超限综合监管',
	},
	{
		from: '渝运安',
		to: '包车客运风险监管',
	},
	{
		from: '渝运安',
		to: '班线客运风险监管',
	},
	{
		from: '渝运安',
		to: '危货运输风险监管',
	},

	{
		from: '普货运输风险监管',
		to: '应急动员',
	},
	{
		from: '货车超限源头监管',
		to: '应急动员',
	},
	{
		from: '货车超限综合监管',
		to: '应急动员',
	},
	{
		from: '包车客运风险监管',
		to: '应急动员',
	},
	{
		from: '班线客运风险监管',
		to: '应急动员',
	},
	{
		from: '危货运输风险监管',
		to: '应急动员',
	},
	{
		from: '数字政务',
		to: '轨道智管',
	},
	{
		from: '轨道智管',
		to: '轨道突发智处',
	},
	{
		from: '轨道突发智处',
		to: '应急动员',
	},
	{
		from: '数字法治',
		to: '打非治违',
	},
	{
		from: '打非治违',
		to: '社会治理',
	},

]

const newList = [];
list.forEach(item => {
	if (item.to.indexOf(',') !== -1) {
		item.to.split(",").forEach(to => {
			newList.push({
				from: item.from,
				to: to
			})
		})
	} else {
		newList.push({
			...item
		})
	}
})
export default newList;