import VueEchartBar from "./bar";
import VueEchartLine from "./line";
import VueEchartPie from "./pie";
import VueEchartPie3D from "./pie-3d";

const components = {
	VueEchartBar,
	VueEchartLine,
	VueEchartPie,
	VueEchartPie3D
}

const install = (app) => {
	Object.keys(components).forEach((name) => {
		app.component(`${name}`, components[name])
	})
}

export default {
	install,
	...components
}
