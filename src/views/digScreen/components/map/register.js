import mapData from './geo-data/500000.json'
import masterData from "./geo-data/master.json";
export const registerMap = (echartInstance) => {
	echartInstance.registerMap("chongqing-geo-map", mapData)

	// 注册主城区合并地图
	let zhuDistrictList = [
		"沙坪坝区",
		"北碚区",
		"渝北区",
		"巴南区",
		"大渡口区",
		"九龙坡区",
		"渝中区",
		"江北区",
		"南岸区",
	];
	let CQGeoJson2 = JSON.parse(JSON.stringify(mapData));
	let features2 = CQGeoJson2.features.filter(item => {
		let falg = zhuDistrictList.find(name => {
			return item.properties.name === name;
		});
		if (falg) {
			return false;
		}
		return true;
	});
	CQGeoJson2.features = [
		masterData,
		...features2
	];
	echartInstance.registerMap('chongqing-geo-master-map', CQGeoJson2);
	// 注册主城区小地图
	let CQGeoJson3 = JSON.parse(JSON.stringify(mapData));
	// console.error('CQGeoJson3',CQGeoJson3)
	let features3 = CQGeoJson3.features.filter(item => {
		let falg = zhuDistrictList.find(name => {
			return item.properties.name === name;
		});

		if (falg) {
			return true;
		}
		return false;
	});
	CQGeoJson3.features = [
		...features3
	];
	echartInstance.registerMap('chongqing-geo-min-map', CQGeoJson3);
}