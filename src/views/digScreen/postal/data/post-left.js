
import csvPremisesData from "./premises.csv";

export const premisesHeaders = [
	...(csvPremisesData[0]?csvPremisesData[0]:[])
];
console.error("premisesHeaders", premisesHeaders)
export const premisesData = csvPremisesData.map((arr, index) => {
	if(index === 0){
		return null;
	}
	// Provinces, cities, districts, and counties Township
	// 省,市,县,乡镇,营业场所名称,详细地址,邮政编码,
	return {
		orderIndex: index, // 序号
		province: arr[0], // 
		city: arr[1], // 
		district: arr[2], //
		township: arr[3], // 
		name: arr[4], // 
		address: arr[5], // 
		code: arr[6], // 
		dataArray: arr
	}
}).filter(item => (item));

// 并且筛选 and
export const requestPostPremisesData = (params) => {
	if(params){
		const keys = Object.keys(params);
		if(keys.length > 0){
			return premisesData.filter(item => {
				let isFilter = true;
				keys.forEach(key => {
					const valueA = params[key];
					const valueB = item[key];
					if(valueA && valueA !== "" && valueA !== valueB){
						isFilter = false;
					}
				})
				return isFilter;
			})
		}
	}
	return premisesData;
}

export const requestPostPremisesFieldData = (field) => {
	const map = {};
	premisesData.forEach(item => {
		const value = item[field];
		if(value && value !== ""){
			map[value] = field;
		}
	})
	return Object.keys(map);
}


// 营业场所
export const postBusinessPremises = [
	{
		name: "渝中区",
		value: 24,
	},
	{
		name: "九龙坡区",
		value: 31,
	},
	{
		name: "南岸区",
		value: 19,
	},
	{
		name: "大渡口区",
		value: 11,
	},
	{
		name: "沙坪坝区",
		value: 32,
	},
	{
		name: "江北区",
		value: 20,
	},
	{
		name: "渝北区",
		value: 20,
	},
	{
		name: "北碚区",
		value: 31,
	},
	{
		name: "长寿区",
		value: 49,
	},
	{
		name: "巴南区",
		value: 54,
	},
	{
		name: "綦江区",
		value: 68,
	},
	{
		name: "江津区",
		value: 110,
	},
	{
		name: "永川区",
		value: 72,
	},
	{
		name: "荣昌区",
		value: 35,
	},
	{
		name: "璧山区",
		value: 42,
	},
	{
		name: "北陵区",
		value: 64,
	},
	{
		name: "丰都县",
		value: 53,
	},
	{
		name: "南川区",
		value: 39,
	},
	{
		name: "垫江县",
		value: 47,
	},
	{
		name: "武隆区",
		value: 30,
	},
	{
		name: "合川区",
		value: 78,
	},
	{
		name: "铜梁区",
		value: 43,
	},
	{
		name: "潼南区",
		value: 46,
	},
	{
		name: "万州区",
		value: 116,
	},
	{
		name: "忠县",
		value: 68,
	},
	{
		name: "开州区",
		value: 71,
	},
	{
		name: "巫山县",
		value: 32,
	},
	{
		name: "巫溪县",
		value: 38,
	},
	{
		name: "奉节县",
		value: 36,
	},
	{
		name: "云阳县",
		value: 63,
	},
	{
		name: "城口县",
		value: 26,
	},
	{
		name: "黔江区",
		value: 32,
	},
	{
		name: "彭水苗族土家族自治县",
		value: 45,
	},
	{
		name: "秀山土家族苗族自治县",
		value: 32,
	},
	{
		name: "酉阳土家族苗族自治县",
		value: 42,
	},
	{
		name: "石柱土家族自治县",
		value: 46,
	},
	{
		name: "大足区",
		value: 41,
	},
	{
		name: "梁平区",
		value: 39,
	},
].map(item => {
	return {
		...item
	}
})

import csvCompaniesData from "./companies.csv";

export const companiesHeaders = [
	...(csvCompaniesData[0]?csvCompaniesData[0]:[])
];
console.error("companiesHeaders", companiesHeaders)
export const companiesData = csvCompaniesData.map((arr, index) => {
	if(index === 0){
		return null;
	}
	return {
		orderIndex: arr[0], // 序号
		name: arr[1], // 单位名称 dwmc
		dwmc: arr[1], // 单位名称 dwmc
		gslx: arr[2], // 公司类型
		zcdz: arr[3], // 注册地址
		fr: arr[4], // 法人
		xkzh: arr[5], // 许可证号
		ppmc: arr[6], // 品牌名称
		ppfl: arr[7], // 品牌名称
		yxq: arr[8], // 有效期
		xkjg: arr[9], // 许可机关
		dataArray: arr
	}
}).filter(item => (item));

// 并且筛选 and
export const requestPostCompaniesData = (params) => {
	if(params){
		const keys = Object.keys(params);
		if(keys.length > 0){
			return companiesData.filter(item => {
				let isFilter = true;
				keys.forEach(key => {
					const valueA = params[key];
					const valueB = item[key];
					if(valueA && valueA !== "" && valueA.indexOf("like ") !== -1){
						const likeValueA = valueA.replace("like ", "");
						// 模糊查询
						if(valueA && valueA !== "" && likeValueA.indexOf(valueB) === -1){
							isFilter = false;
						}
					}else{
						if(valueA && valueA !== "" && valueA !== valueB){
							isFilter = false;
						}
					}
				})
				return isFilter;
			})
		}
	}
	return companiesData;
}
export const requestPostCompanieFieldData = (field) => {
	const map = {};
	companiesData.forEach(item => {
		const value = item[field];
		if(value && value !== ""){
			map[value] = field;
		}
	})
	return Object.keys(map);
}

// 快递企业
export const postExpressDeliveryCompanies = companiesData.map((item) => {
	return {
		...item,
		value: ""//0
	}
});


// 集邮市场
export const postPhilatelicMarket = [
	{
		name: "重庆中兴路旧货收藏品交易市场",
		address: "重庆市渝中区中兴路73号",
		value: ""
	}
];