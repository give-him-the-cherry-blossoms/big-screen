//公共交通请求地址
import { GDPM, GJPM, MRKLL, CZCDR, RailTransit } from "@/services/api"
import { request, METHOD, requestJson } from "@/utils/request"
const BASE_URL = process.env.VUE_APP_URL //运输-轨道

// 安全管理
export async function getAqglData(params) {
    return request(BASE_URL + "/dataScreen/shouye/aqgl", METHOD.GET, params)
}

// 综合执法
export async function getZhzf(params) {
    return request(BASE_URL + "/dataScreen/shouye/zhzf", METHOD.GET, params)
}

// 客运服务
export async function getKyfw(params) {
    return request(BASE_URL + "/dataScreen/shouye/kyfw", METHOD.GET, params)
}

// 货运服务
export async function getHyfw(params) {
    return request(BASE_URL + "/dataScreen/shouye/hyfw", METHOD.GET, params)
}
