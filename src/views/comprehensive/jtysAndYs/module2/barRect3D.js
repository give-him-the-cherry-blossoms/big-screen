let state = {
  yAxisData: [19, 381, 1735, 166, 33],
  yAxisData1: [1],
};
export const option3d = {
  // 柱状图
  bar3dOption(xData, sData, barWidth = 30) {
    const option = {
      grid: {
        top: 20,
        left: 20,
        right: 20,
        bottom: 20,
      },
      xAxis: {
        data: xData ? xData : ["-", "-"],
        axisLine: {
          show: false, // 隐藏Y轴线
        },
        axisLabel: {
          inside: false,
          textStyle: {
            color: "#B7F1FF",
          },
          axisTick: {
            show: false,
          },
        },
      },
      yAxis: {
        axisLine: {
          show: false, // 隐藏Y轴线
        },
        axisLabel: {
          show: false,
          axisTick: {
            show: false,
          },
        },
        splitLine: {
          show: false,
          lineStyle: {
            // 分割线的颜色
            color: "#022B56",
            // 分割线的宽度
            width: 1,
            // 分割线的类型
            type: "solid",
          },
        },
      },
      series: [
        {
          data: sData ? sData : state.yAxisData,
          stack: "zs",
          type: "bar",
          // barMaxWidth: "auto",
          barWidth: barWidth,
          label: {
            show: true, // 显示标签
            position: "top", // 标签位置在顶部
            color: "#23FFFC",
          },
          itemStyle: {
            color: {
              x: 0,
              y: 0,
              x2: 0,
              y2: 1,
              type: "linear",
              global: false,
              colorStops: [
                {
                  offset: 0,
                  color: "#5EFAFA",
                },
                {
                  offset: 1,
                  color: "#349BBD",
                },
              ],
            },
          },
        },

        //下面的立体,控制颜色是color第一个
        {
          data: state.yAxisData1,
          type: "pictorialBar",
          symbol: "diamond",
          symbolOffset: [0, "50%"],
          symbolSize: [barWidth, barWidth / 5],
          zlevel: 2,
          itemStyle: {
            color: "#359CBF",
          },
        },
        //上面的立体,控制颜色是color第二个
        {
          data: sData ? sData : state.yAxisData,
          type: "pictorialBar",
          symbolPosition: "end",
          symbol: "diamond",
          symbolOffset: [0, "-50%"],
          symbolSize: [barWidth, barWidth / 2 - (barWidth / 5 + 2)],
          zlevel: 2,
          itemStyle: {
            color: "#38E3FF",
          },
        },
      ],
    };
    return option;
  },
};

export const colors = [
  "#0167F5",
  "rgba(221, 162, 0,1)",
  "#20bf6b",
  "#e17055",
  "#8e44ad",
  "#3742fa",
  "#1289a7",
];

export const stepColor = ["rgba(6, 82, 221,0.5)", "rgba(0, 144, 221,0.5)"];

export const stepColor2 = ["#0167F5", "#03E9D8"];

// 返回  近7日的日期

export const days7List = () => {
  const today = new Date();
  const dates = [];
  for (let i = 0; i < 7; i++) {
    const date = new Date(today);
    date.setDate(today.getDate() - i);
    dates.push(date.toISOString().split("T")[0]);
  }
  return dates.reverse();
};

export const gsDateList = () => {
  return days7List().map((item) => {
    let temp = item.split("-");
    return Number(temp[1]) + "/" + Number(temp[2]);
  });
};

export const fontSize = 16;
