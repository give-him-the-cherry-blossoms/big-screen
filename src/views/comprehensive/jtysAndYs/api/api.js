/*
 * @Author: yulinZ 1973329248@qq.com
 * @Date: 2024-05-25 10:19:35
 * @LastEditors: yulinZ 1973329248@qq.com
 * @LastEditTime: 2024-05-25 21:32:22
 * @FilePath: \daping_danao_web\src\views\comprehensive\jtysAndYs\api\api.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
//公共交通请求地址
import { GDPM, GJPM, MRKLL, CZCDR, RailTransit } from "@/services/api"
import { request, METHOD, requestJson } from "@/utils/request"
const BASE_URL = process.env.VUE_APP_URL //运输-轨道
const BASE_URL1 = process.env.VUE_APP_URL1 //运输-轨道
export async function getGdData(params) {
    console.log(BASE_URL, "BASE_URL")
    return request(BASE_URL + "/dataScreen/yunshu/gd", METHOD.GET, params)
}

//出租车
export async function getCzc(params) {
    return request(BASE_URL + "/dataScreen/yunshu/czc", METHOD.GET, params)
}

///dataScreen/yunshu/bxky
export async function getBxky(params) {
    return request(BASE_URL + "/dataScreen/yunshu/bxky", METHOD.GET, params)
}

// 多数据合集
export async function getDbfx(params) {
    return request(BASE_URL + "/dataScreen/yunshu/dbfx", METHOD.GET, params)
}

// 小时数据集合
export async function getXsdata(params) {
    return request(BASE_URL + "/dataScreen/yunshu/xsdata", METHOD.GET, params)
}

//  多天数据集合，截至到今天
export async function getLcsxs(params) {
    return request(BASE_URL + "/dataScreen/yunshu/lcsxs", METHOD.GET, params)
}

// 网约车饱和率弹窗
export async function getTcbhdxq(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tcbhdxq", METHOD.GET, params)
}

// 出租车  昨日客流 弹窗
export async function getTcdds(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tcdds", METHOD.GET, params)
}

// 网约车  昨日客流 弹窗
export async function getTcqzjd(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tcqzjd", METHOD.GET, params)
}

// 网约车当日  订单 弹窗
export async function getTczjd(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tczjd", METHOD.GET, params)
}

// 网约车 平台详情 弹窗
export async function getTcxkmx(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tcxkmx", METHOD.GET, params)
}

// 出租车 实载率
export async function getTcxsl(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tcxsl", METHOD.GET, params)
}

// 出租车 稳定度
export async function getTcxycz(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tcxycz", METHOD.GET, params)
}

// 公交 昨日客流
export async function getTczkll(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tczkll", METHOD.GET, params)
}

// 全市班线客运量
export async function getQsybxkyl(params) {
    return request(BASE_URL + "/dataScreen/yunshu/qsybxkyl", METHOD.GET, params)
}

// 网约车行业饱和度-健康度
export async function getWycbhd(params) {
    return request(BASE_URL + "/dataScreen/yunshu/wycbhd", METHOD.GET, params)
}

// 人次比列表
export async function getTcrcb(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tcrcb", METHOD.GET, params)
}

// 人次比列表详情
export async function getTcrcbxq(params) {
    return request(BASE_URL + "/dataScreen/yunshu/tcrcbxq", METHOD.GET, params)
}

// 正点率 "http://10.224.168.16" +
export async function getZdl(params) {
    return requestJson(
        BASE_URL1 + "/gdzg/service/S_ZC_FB_01",
        METHOD.POST,
        params
    )
}

// 列车上线 数
export async function getKll(params) {
    return requestJson(
        BASE_URL1 + "/gdzg/service/S_ZC_FB_03",
        METHOD.POST,
        params
    )
}

// 轨道上线数 /gdzg/service/S_ZC_FB_02
export async function getSxs(params) {
    return requestJson(
        BASE_URL1 + "/gdzg/service/S_ZC_FB_02",
        METHOD.POST,
        params
    )
}

// 考试合格率
export async function getHgl(params) {
    return requestJson("/openApi/jt/queryBasic", METHOD.GET, params)
}

// 维修统计新接口
export async function getWxcs(params) {
    return request(BASE_URL + "/dataScreen/yunshu/wxcs", METHOD.GET, params)
}
