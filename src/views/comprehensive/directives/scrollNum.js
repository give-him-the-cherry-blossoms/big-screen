import { ref } from "vue";
// 数字滚动增长自定义指令
let timer = null;
let currentUpValue = 0;
export const scrollNumber = {
  mounted(el, binding) {
    let timer = null;
    const target = Number(binding.value);
    currentUpValue = target;
    const duration = 1000; // 持续时间为 1 秒钟
    const step = Math.ceil(target / (duration / 16)); // 步长为总距离除以时间
    console.log(step, "step");
    let current = 0;
    timer = setInterval(() => {
      current += step;
      if (current >= target) {
        current = target;
        clearInterval(timer);
      }
      el.innerHTML = current.toLocaleString(); // 用 toLocaleString 将数值格式化为千分位表示
    }, 16); // 每隔 16 毫秒变化一次
    // el.innerHTML = current.toLocaleString(); // 用 toLocaleString 将数值格式化为千分位表示
  },
  updated(el, binding) {
    let timer = null;
    const target = Number(binding.value);

    const duration = 2000; // 持续时间为 1 秒钟
    let step = Math.ceil(target / (duration / 16)); // 步长为总距离除以时间

    let current = currentUpValue;
    if (target - current < 2) {
      step = 0.01;
    }
    console.log(step, "step");

    timer = setInterval(() => {
      current += step;
      if (current >= target) {
        current = target;
        clearInterval(timer);
      }
      el.innerHTML = current.toLocaleString(); // 用 toLocaleString 将数值格式化为千分位表示
    }, 16); // 每隔 16 毫秒变化一次
    currentUpValue = target;
  },
  unmounted() {
    clearInterval(timer);
  },
};
