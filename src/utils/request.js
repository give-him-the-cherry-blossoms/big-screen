import axios from "axios";

// 跨域认证信息 header 名
const xsrfHeaderName = "Authorization";
const Qs = require("qs");
axios.defaults.timeout = 5000;
axios.defaults.withCredentials = true;
axios.defaults.xsrfHeaderName = xsrfHeaderName;

// 认证类型
const AUTH_TYPE = {
  BEARER: "Bearer",
  BASIC: "basic",
  AUTH1: "auth1",
  AUTH2: "auth2",
};

// http method
const METHOD = {
  GET: "get",
  POST: "post",
};

/**
 * axios请求
 * @param url 请求地址
 * @param method {METHOD} http method
 * @param params 请求参数
 * @returns {Promise<AxiosResponse<T>>}
 * X-access-call: '0f70ce78d82d5495f48b3f66ecbdbc30' 为项目中前后端匹配key-value
 */
async function request(url, method, params) {
  console.log(url);
  // params['param_access_call']='0f70ce78d82d5495f48b3f66ecbdbc30'
  switch (method) {
    case METHOD.GET:
      return axios.get(url, { params, headers: {} });
    //post form-data先序列化
    case METHOD.POST:
      return axios.post(url, Qs.stringify(params), {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      });
    default:
      return axios.get(url, { params, headers: {} });
  }
}
async function requestJson(url, method, params) {
  console.log(url);
  // params['param_access_call']='0f70ce78d82d5495f48b3f66ecbdbc30'
  switch (method) {
    case METHOD.GET:
      return axios.get(url, { params, headers: {} });
    //post form-data先序列化
    case METHOD.POST:
      return axios.post(url, params, {
        headers: {
          "Content-Type": "application/json;charset=UTF-8",
        },
      });
    default:
      return axios.get(url, { params, headers: {} });
  }
}

/**
 * 加载 axios 拦截器
 * @param interceptors
 * @param options
 */
function loadInterceptors(interceptors, options) {
  const { request, response } = interceptors;
  // 加载请求拦截器
  request.forEach((item) => {
    let { onFulfilled, onRejected } = item;
    if (!onFulfilled || typeof onFulfilled !== "function") {
      onFulfilled = (config) => config;
    }
    if (!onRejected || typeof onRejected !== "function") {
      onRejected = (error) => Promise.reject(error);
    }
    axios.interceptors.request.use(
      (config) => onFulfilled(config, options),
      (error) => onRejected(error, options)
    );
  });
  // 加载响应拦截器
  response.forEach((item) => {
    let { onFulfilled, onRejected } = item;
    if (!onFulfilled || typeof onFulfilled !== "function") {
      onFulfilled = (response) => response;
    }
    if (!onRejected || typeof onRejected !== "function") {
      onRejected = (error) => Promise.reject(error);
    }
    axios.interceptors.response.use(
      (response) => onFulfilled(response, options),
      (error) => onRejected(error, options)
    );
  });
}

/**
 * 解析 url 中的参数
 * @param url
 * @returns {Object}
 */
function parseUrlParams(url) {
  const params = {};
  if (!url || url === "" || typeof url !== "string") {
    return params;
  }
  const paramsStr = url.split("?")[1];
  if (!paramsStr) {
    return params;
  }
  const paramsArr = paramsStr.replace(/&|=/g, " ").split(" ");
  for (let i = 0; i < paramsArr.length / 2; i++) {
    const value = paramsArr[i * 2 + 1];
    params[paramsArr[i * 2]] =
      value === "true" ? true : value === "false" ? false : value;
  }
  return params;
}

export {
  METHOD,
  AUTH_TYPE,
  request,
  loadInterceptors,
  parseUrlParams,
  requestJson,
};
