/*
 * @Author: yulinZ 1973329248@qq.com
 * @Date: 2024-05-25 10:19:35
 * @LastEditors: yulinZ 1973329248@qq.com
 * @LastEditTime: 2024-05-25 10:44:19
 * @FilePath: \daping_danao_web\vue.config.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const { defineConfig } = require("@vue/cli-service")
const path = require("path") //导入路径插件
// production 生产环境   development 本地环境
let isProduction = process.env.NODE_ENV == "production", //判断当前的环境
    baseUrl //定义baseUrl
if (isProduction) {
    // 生产环境
    baseUrl = process.env.VUE_APP_URL //当前就是生产环境baseUrl的地址
} else {
    baseUrl = process.env.VUE_APP_URL //当前就是开发环境baseUrl的地址
}
console.error("defineConfig", defineConfig)
module.exports = {
    chainWebpack: config => {
        config.module
            .rule("csv")
            .test(/\.(csv|tsv)$/)
            .use()
            .loader("csv-loader")
            .end()
    },
    configureWebpack: {
        module: {
            rules: [
                {
                    test: path.resolve(__dirname, 'node_modules/leader-line/'),
                    use: [{
                        loader: 'skeleton-loader',
                        options: { procedure: content => `${content}export default LeaderLine` }
                    }]
                }
            ]
        }
    },
    // configureWebpack: {
    // 	...configureWebpack,
    // 	module: {
    // 		rules: [{
    // 			test: /\.(csv|tsv)$/,
    // 			use: "csv-loader"
    // 		}, ]
    // 	}
    // },
    // publicPath: "./", //'/visualizat/', //./：相对路径，history.pushState时避免使用相对路径
    publicPath: process.env.NODE_ENV == "development" ? "/" : "/", //./：相对路径，history.pushState时避免使用相对路径
    // 当前配置就是打包后输出的文件夹，以便区分，所以做如下配置
    outputDir: process.env.NODE_ENV == "development" ? "dist" : "dist", // 打包生成目录
    devServer: {
        client: {
            overlay: false,
        },
        proxy: {
            //配置代理代理
            "/baseurl": {
                // '^/api'别名（你的接口是以什么开头的就更换成什么，例如：^/orrce）
                // target: 'http://192.168.11.162:9800',
                target: process.env.VUE_APP_URL,
                ws: true, // 是否允许跨域
                changeOrigin: true,
                secure: true,
                pathRewrite: {
                    "^/portal-api": "/",
                },
            },
            "/dataScreen": {
                target: process.env.VUE_APP_URL + "/dataScreen", // "http://10.224.168.33:8110/dataScreen",
                // target: "http://192.168.1.207:8110/dataScreen",
                changeOrigin: true,
                pathRewrite: {
                    "^/dataScreen": "",
                },
            },
            "/gdzg": {
                target: "http://10.224.168.16",
                changeOrigin: true,
                pathRewrite: {
                    "^/gdzg": "/gdzg",
                },
            },
            "/openApi": {
                target: "http://100.92.4.71:19203/openApi",
                changeOrigin: true,
                pathRewrite: {
                    "^/openApi": "",
                },
            },
        },
        // webpack项目太卡了，关闭 hot
        hot: false, //保存实时刷新
    },
    css: {
        extract: false, // 不单独分包，注入inline 打包后 线上样式与本地不一致
        sourceMap: false,
        loaderOptions: {
            css: {},
            scss: {},
            less: {
                // javascriptEnabled: true
            },
        },
    },
}
